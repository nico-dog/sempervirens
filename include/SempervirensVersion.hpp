#ifndef SEMPERVIRENSVERSION_HPP
#define SEMPERVIRENSVERSION_HPP
#include <string>

std::string getSempervirensVersion();
unsigned int getSempervirensVersionMajor();
unsigned int getSempervirensVersionMinor();
unsigned int getSempervirensVersionPatch();
unsigned int getSempervirensVersionTweak();
#endif
