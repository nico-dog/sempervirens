#ifndef SEMPERVIRENS_HPP
#define SEMPERVIRENS_HPP

// For use by Sempervirens applications.
#include <Sempervirens_pch.hpp>
#include <Application/EntryPoint.hpp> // includes Application/Application.hpp

#endif
