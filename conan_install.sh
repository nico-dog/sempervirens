#!/bin/sh

config=$1
platform=$2

if [ $platform != "Linux" ]
then
    if [ $platform != "apple" ]
    then
	echo "unrecognized platform"
	return
    fi
fi

builddir="build-Sempervirens-"$config'-'$platform
rm -rf ../$builddir
mkdir ../$builddir && cd ../$builddir

conan install ../Sempervirens

