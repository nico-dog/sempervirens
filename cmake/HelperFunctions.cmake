cmake_minimum_required(VERSION 3.19 FATAL_ERROR)

function(create_unittest)
    # Note: targets reuse the main project precompiled header and so must use the exact same
    # compilation flags for it to be valid
    set(prefix       ARG)
    set(noValues     "")
    set(singleValues DIR)
    set(multiValues  DEP)
    cmake_parse_arguments(${prefix} "${noValues}" "${singleValues}" "${multiValues}" ${ARGN})
    set(DIR ${${prefix}_DIR})
    set(DEP ${${prefix}_DEP})

    string(TOLOWER ${DIR} dir)
    set(TGT "test_${dir}")
    add_executable(${TGT})
    target_sources(${TGT} PRIVATE ${DIR}/${DIR}Test.cpp)
    target_precompile_headers(${TGT} REUSE_FROM ${CMAKE_PROJECT_NAME})
    foreach(dep IN ITEMS ${DEP})
    	target_link_libraries(${TGT} PRIVATE ${dep})
    endforeach()
    target_compile_options(${TGT}
	PRIVATE
	-Wall -O2)
    add_test(NAME test-${dir} COMMAND ${TGT})
endfunction()