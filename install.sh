#!/bin/sh
config=$1
platform=$2
if [ $platform != "Linux" ]
then
    if [ $platform != "apple" ]
    then
	echo "unrecognized platform"
	return
    fi
fi

installdir="/home/nico/Desktop/SempervirensInstall"
#installdir=$3
mkdir -p $installdir

builddir="build-Sempervirens-"$config'-'$platform
cd ../$builddir
cmake --install . --prefix $installdir

