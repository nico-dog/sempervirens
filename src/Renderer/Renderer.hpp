#ifndef RENDERER_HPP
#define RENDERER_HPP
#include <Macros/Build.hpp>
#include <Macros/Platform.hpp>

#if SEMPERVIRENS_PLATFORM(UNIX)
#if SEMPERVIRENS_BUILD(OPENGL)
#include <Renderer/Platform/Linux/Backend/GLRenderer.hpp>
using Renderer_t = sempervirens::renderer::GLRenderer;
#endif
#endif
#endif
