#define GLRENDERER_CPP
#include <Logging/Logging.hpp>
#include <Timer/Timer.hpp>
#include <Renderer/Platform/Linux/Backend/GLRenderer.hpp>
#include <GL/glx.h>
using namespace sempervirens::timer;
using namespace sempervirens::eventsystem;

namespace sempervirens::renderer {
GLRenderer::GLRenderer() {
  SEMPERVIRENS_MSG("GLRenderer ctor, size of GLRenderer: %d\n", sizeof(GLRenderer));
  SEMPERVIRENS_LISTEN(this, WindowExposeEvent);
  SEMPERVIRENS_LISTEN(this, WindowResizeEvent);
  loadGL();
  init();
}
GLRenderer::~GLRenderer() {
  SEMPERVIRENS_MSG("GLRenderer dtor\n");
  glDeleteProgram(_rdrProgInfo._progID);
  glDeleteProgram(_cmpProgInfo._progID);
  glDeleteTextures(1, &_cmpProgInfo._texID);
}

void GLRenderer::loadGL() {
  glCreateShader = (PFNGLCREATESHADERPROC)glXGetProcAddressARB((const GLubyte*)"glCreateShader");
  glShaderSource = (PFNGLSHADERSOURCEPROC)glXGetProcAddressARB((const GLubyte*)"glShaderSource");
  glCompileShader = (PFNGLCOMPILESHADERPROC)glXGetProcAddressARB((const GLubyte*)"glCompileShader");
  glGetShaderiv = (PFNGLGETSHADERIVPROC)glXGetProcAddressARB((const GLubyte*)"glGetShaderiv");
  glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)glXGetProcAddressARB((const GLubyte*)"glGetShaderInfoLog");
  glCreateProgram = (PFNGLCREATEPROGRAMPROC)glXGetProcAddressARB((const GLubyte*)"glCreateProgram");
  glAttachShader = (PFNGLATTACHSHADERPROC)glXGetProcAddressARB((const GLubyte*)"glAttachShader");
  glLinkProgram = (PFNGLLINKPROGRAMPROC)glXGetProcAddressARB((const GLubyte*)"glLinkProgram");
  glGetProgramiv = (PFNGLGETPROGRAMIVPROC)glXGetProcAddressARB((const GLubyte*)"glGetProgramiv");
  glGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC)glXGetProcAddressARB((const GLubyte*)"glGetProgramInfoLog");
  glDeleteShader = (PFNGLDELETESHADERPROC)glXGetProcAddressARB((const GLubyte*)"glDeleteShader");
  glGenTextures = (PFNGLGENTEXTURESPROC)glXGetProcAddressARB((const GLubyte*)"glGenTextures");
  glActiveTexture = (PFNGLACTIVETEXTUREPROC)glXGetProcAddressARB((const GLubyte*)"glActiveTexture");
  glBindTexture = (PFNGLBINDTEXTUREPROC)glXGetProcAddressARB((const GLubyte*)"glBindTexture");
  glTexParameteri = (PFNGLTEXPARAMETERIPROC)glXGetProcAddressARB((const GLubyte*)"glTexParameteri");
  glTexImage2D = (PFNGLTEXIMAGE2DPROC)glXGetProcAddressARB((const GLubyte*)"glTexImage2D");
  glBindImageTexture = (PFNGLBINDIMAGETEXTUREPROC)glXGetProcAddressARB((const GLubyte*)"glBindImageTexture");
  glGetIntegeri_v = (PFNGLGETINTEGERI_VPROC)glXGetProcAddressARB((const GLubyte*)"glGetIntegeri_v");
  glGetIntegerv = (PFNGLGETINTEGERVPROC)glXGetProcAddressARB((const GLubyte*)"glGetIntegerv");
  glGetString = (PFNGLGETSTRINGPROC)glXGetProcAddressARB((const GLubyte*)"glGetString");
  glGetStringi = (PFNGLGETSTRINGIPROC)glXGetProcAddressARB((const GLubyte*)"glGetStringi");
  glDispatchCompute = (PFNGLDISPATCHCOMPUTEPROC)glXGetProcAddressARB((const GLubyte*)"glDispatchCompute");
  glMemoryBarrier = (PFNGLMEMORYBARRIERPROC)glXGetProcAddressARB((const GLubyte*)"glMemoryBarrier");
  glUseProgram = (PFNGLUSEPROGRAMPROC)glXGetProcAddressARB((const GLubyte*)"glUseProgram");
  glUniform1i = (PFNGLUNIFORM1IPROC)glXGetProcAddressARB((const GLubyte*)"glUniform1i");
  glUniform1f = (PFNGLUNIFORM1FPROC)glXGetProcAddressARB((const GLubyte*)"glUniform1f");
  glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)glXGetProcAddressARB((const GLubyte*)"glGetUniformLocation");
  glDeleteProgram = (PFNGLDELETEPROGRAMPROC)glXGetProcAddressARB((const GLubyte*)"glDeleteProgram");
  glGetError = (PFNGLGETERRORPROC)glXGetProcAddressARB((const GLubyte*)"glGetError");
  glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)glXGetProcAddressARB((const GLubyte*)"glGenVertexArrays");
  glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)glXGetProcAddressARB((const GLubyte*)"glBindVertexArray");
}

void GLRenderer::init() {
  int major = 0;
  int minor = 0;
  glGetIntegerv(GL_MAJOR_VERSION, &major);
  glGetIntegerv(GL_MINOR_VERSION, &minor);
  SEMPERVIRENS_MSG("OpenGL context created with\nVersion %d.%d (%s)\nVendor %s\nRenderer %s\n",
		   major, minor,
		   glGetString(GL_VERSION),
		   glGetString(GL_VENDOR),
		   glGetString(GL_RENDERER));
  int nExtensions = 0;
  glGetIntegerv(GL_NUM_EXTENSIONS, &nExtensions);
  SEMPERVIRENS_MSG("Number of extensions supported: %d\n", nExtensions);
  //for (auto i = 0; i < nExtensions; ++i) {
  //  SEMPERVIRENS_MSG("%s\n", glGetStringi(GL_EXTENSIONS, i));
  //}
  
  // Vertex shader
  // Defines full screen triangle vertices at (-1,-1), (3,-1), (-1,3)
  // Triangle fully covers the clip space quad over [-1, 1] range
  // To get the normalized text coordinates in the [0, 1] range
  // one returns position values over [0, 2] 
  const char* vsCode[] = {
  //_rdrProgInfo._vsCode = {
    "#version 430                                      \n"
    "out vec2 uv;                                      \n"
    "void main() {                                     \n"
    "  float x = -1.0 + float((gl_VertexID & 1) << 2); \n"
    "  float y = -1.0 + float((gl_VertexID & 2) << 1); \n"
    "  uv.x = (x + 1.0) * 0.5;                         \n"
    "  uv.y = (y + 1.0) * 0.5;		               \n"
    "  gl_Position = vec4(x, y, 0, 1);		       \n"
    "}                                                 \n"
  };
  _rdrProgInfo._vsID = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(_rdrProgInfo._vsID, 1, vsCode, nullptr);
  glCompileShader(_rdrProgInfo._vsID);
  int success = 0;
  glGetShaderiv(_rdrProgInfo._vsID, GL_COMPILE_STATUS, &success);
  if (!success) {
    char log[512];
    glGetShaderInfoLog(_rdrProgInfo._vsID, 512, nullptr, log);
    SEMPERVIRENS_ERR("Vertex shader compilation failed:\n");
    SEMPERVIRENS_ERR("%s\n", log);
  }
  glCheckError();
 
  // Fragment shader
  const char* fsCode[] = {  
    "#version 430 core                  \n"
    "out vec4 color;                    \n"
    "in vec2 uv;                        \n"
    "uniform sampler2D cmpTexture;      \n"
    "void main() {                      \n"
    "  color = texture(cmpTexture, uv); \n"
    "}                                  \n"
  };
  _rdrProgInfo._fsID = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(_rdrProgInfo._fsID, 1, fsCode, nullptr);
  glCompileShader(_rdrProgInfo._fsID);
  success = 0;
  glGetShaderiv(_rdrProgInfo._fsID, GL_COMPILE_STATUS, &success);
  if (!success) {
    char log[512];
    glGetShaderInfoLog(_rdrProgInfo._fsID, 512, nullptr, log);
    SEMPERVIRENS_ERR("Fragment shader compilation failed:\n");
    SEMPERVIRENS_ERR("%s\n", log);
  }
  glCheckError();
  
  // Render program
  _rdrProgInfo._progID = glCreateProgram();
  glAttachShader(_rdrProgInfo._progID, _rdrProgInfo._vsID);
  glAttachShader(_rdrProgInfo._progID, _rdrProgInfo._fsID);
  glLinkProgram(_rdrProgInfo._progID);
  success = 0;
  glGetProgramiv(_rdrProgInfo._progID, GL_LINK_STATUS, &success);
  if(!success) {
    char log[512];
    glGetProgramInfoLog(_rdrProgInfo._progID, 512, nullptr, log);
    SEMPERVIRENS_ERR("Program linking failed:\n");
    SEMPERVIRENS_ERR("%s\n", log);
  }
  // Core OpenGL requires to bind a VAO before drawing any vertex, even an empty one
  glGenVertexArrays(1, &_rdrProgInfo._VAO);  
  glCheckError();
  
  // Can delete shaders as they are now linked to the program
  glDeleteShader(_rdrProgInfo._vsID);
  glDeleteShader(_rdrProgInfo._fsID);
  
  // Set up the texture the compute shader will write to
  _cmpProgInfo._texWidth = 512;
  _cmpProgInfo._texHeight = 512;
  glGenTextures(1, &_cmpProgInfo._texID);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, _cmpProgInfo._texID);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, _cmpProgInfo._texWidth, _cmpProgInfo._texHeight, 0, GL_RGBA, GL_FLOAT, nullptr);
  glCheckError();
  
  // Work groups for compute shader
  int workGroupCount[3] = {0, 0, 0};
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &workGroupCount[0]);
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &workGroupCount[1]);
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &workGroupCount[2]);
  SEMPERVIRENS_MSG("Max global work group counts:\n");
  SEMPERVIRENS_MSG("x: %i, y: %i, z: %i\n", workGroupCount[0], workGroupCount[1], workGroupCount[2]);
  int workGroupSize[3] = {0, 0, 0};
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &workGroupSize[0]);
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &workGroupSize[1]);
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &workGroupSize[2]);
  SEMPERVIRENS_MSG("Max local work group sizes:\n");
  SEMPERVIRENS_MSG("x: %i, y: %i, z: %i\n", workGroupSize[0], workGroupSize[1], workGroupSize[2]);
  int workGroupInv = 0;
  glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &workGroupInv);
  SEMPERVIRENS_MSG("Max local work group invocations: %i\n", workGroupInv);
  glCheckError();
  
  // Compute shader
  const char* csCode[] = {
    "#version 430                                             \n"
    "layout(local_size_x = 1, local_size_y = 1) in;           \n"
    "layout(rgba32f, binding = 0) uniform image2D img_output; \n"
    "uniform float utime;                                      \n"
    "void main() {                                            \n"
    "  //vec4 pixel = vec4(abs(sin(utime/500.)), 0.0, 0.0, 1.0);      \n"
    "  vec4 pixel = vec4(1.0, 0.0, 0.0, 1.0);      \n"
    "  ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);  \n"
    "  // cool stuff here                                     \n"
    "  imageStore(img_output, pixel_coords, pixel);	      \n"
    "}                                                        \n"
  };
  _cmpProgInfo._csID = glCreateShader(GL_COMPUTE_SHADER);
  glShaderSource(_cmpProgInfo._csID, 1, csCode, nullptr);
  glCompileShader(_cmpProgInfo._csID);
  success = 0;
  glGetShaderiv(_cmpProgInfo._csID, GL_COMPILE_STATUS, &success);
  if (!success) {
    char log[512];
    glGetShaderInfoLog(_cmpProgInfo._csID, 512, nullptr, log);
    SEMPERVIRENS_ERR("Compute shader compilation failed:\n");
    SEMPERVIRENS_ERR("%s\n", log);
  }
  glCheckError();
  
  // Compute shader program
  _cmpProgInfo._progID = glCreateProgram();
  glAttachShader(_cmpProgInfo._progID, _cmpProgInfo._csID);
  glLinkProgram(_cmpProgInfo._progID);
  success = 0;
  glGetProgramiv(_cmpProgInfo._progID, GL_LINK_STATUS, &success);
  if(!success) {
    char log[512];
    glGetProgramInfoLog(_cmpProgInfo._progID, 512, nullptr, log);
    SEMPERVIRENS_ERR("Compute program linking failed:\n");
    SEMPERVIRENS_ERR("%s\n", log);
  }
  glCheckError();
  
  // Can delete shader as it is now linked to the program
  glDeleteShader(_cmpProgInfo._csID);
}

GLenum GLRenderer::glCheckError() {
  GLenum errorCode;
  while ((errorCode = glGetError()) != GL_NO_ERROR) {
        switch (errorCode) {
	case GL_INVALID_ENUM:
	  SEMPERVIRENS_ERR("INVALID_ENUM\n"); break;
	case GL_INVALID_VALUE:
	  SEMPERVIRENS_ERR("INVALID_VALUE\n"); break;
	case GL_INVALID_OPERATION:
	  SEMPERVIRENS_ERR("INVALID_OPERATION\n"); break;
	case GL_STACK_OVERFLOW:
	  SEMPERVIRENS_ERR("STACK_OVERFLOW\n"); break;
	case GL_STACK_UNDERFLOW:
	  SEMPERVIRENS_ERR("STACK_UNDERFLOW\n"); break;
	case GL_OUT_OF_MEMORY:
	  SEMPERVIRENS_ERR("OUT_OF_MEMORY\n"); break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
	  SEMPERVIRENS_ERR("INVALID_FRAMEBUFFER_OPERATION\n"); break;
        }
  }
  return errorCode;
}  
  
void GLRenderer::onEvent(Event& event) {
  if (event.type() == EventType::WindowExposed || event.type() == EventType::WindowResized) {
    auto e = static_cast<WindowExposeEvent&>(event);
    SEMPERVIRENS_MSG("Renderer received WindowExposed or WindowResized event type\n");
    glViewport(0, 0, e._width, e._height);
    return;
  }
}

void GLRenderer::render() {
  // Compute pass
  glUseProgram(_cmpProgInfo._progID);

  //auto now = getTimeInMus();
  //SEMPERVIRENS_MSG("time %d\n", now);
  //glUniform1f(glGetUniformLocation(_cmpProgInfo._progID, "utime"), now);
  
  glBindImageTexture(0, _cmpProgInfo._texID, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
  glDispatchCompute((GLuint)_cmpProgInfo._texWidth, (GLuint)_cmpProgInfo._texHeight, 1);
  
  // Make sure writing to image has finished before read
  glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
  
  // Drawing pass
  glClear(GL_COLOR_BUFFER_BIT);
  glUseProgram(_rdrProgInfo._progID);
  glBindVertexArray(_rdrProgInfo._VAO);
  //glActiveTexture(GL_TEXTURE0); 
  //glBindTexture(GL_TEXTURE_2D, _cmpProgInfo._texID); 
  glDrawArrays(GL_TRIANGLES, 0, 3);
  
  glCheckError();
}

}
