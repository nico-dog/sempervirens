#ifndef GLRENDERER_HPP
#define GLRENDERER_HPP
#include <EventSystem/Event.hpp>
// glcorearb.h needs to be included before gl.h to load the core functions
// gl.h will include glext.h for the extensions
#include<GL/glcorearb.h>
#include<GL/gl.h>

namespace sempervirens::renderer {

struct RenderShaderProgInfo {
  char const* _vsCode{nullptr};
  char const* _fsCode{nullptr};
  GLuint _vsID{0};
  GLuint _fsID{0};
  GLuint _progID{0};
  unsigned int _VAO; 
};
struct ComputeShaderProgInfo {
  char const* _csCode{nullptr};
  GLuint _csID{0};
  GLuint _progID{0};
  GLuint _texID{0};
  int _texWidth{0};
  int _texHeight{0}; 
};
  
class GLRenderer {
public:
  GLRenderer();
  ~GLRenderer();
  void render();
  void onEvent(sempervirens::eventsystem::Event &event);
  
private:
  void loadGL();
  void init();
  
  GLenum glCheckError(); // 4B
  // OpenGL function pointers to be loaded dynamically
  PFNGLCREATESHADERPROC glCreateShader{nullptr};  // 8B
  PFNGLSHADERSOURCEPROC glShaderSource{nullptr};
  PFNGLCOMPILESHADERPROC glCompileShader{nullptr};
  PFNGLGETSHADERIVPROC glGetShaderiv{nullptr};
  PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog{nullptr};
  PFNGLCREATEPROGRAMPROC glCreateProgram{nullptr};
  PFNGLATTACHSHADERPROC glAttachShader{nullptr};
  PFNGLLINKPROGRAMPROC glLinkProgram{nullptr};
  PFNGLGETPROGRAMIVPROC glGetProgramiv{nullptr};
  PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog{nullptr};
  PFNGLDELETESHADERPROC glDeleteShader{nullptr};
  PFNGLGENTEXTURESPROC glGenTextures{nullptr};
  PFNGLACTIVETEXTUREPROC glActiveTexture{nullptr};
  PFNGLBINDTEXTUREPROC glBindTexture{nullptr};
  PFNGLTEXPARAMETERIPROC glTexParameteri{nullptr};
  PFNGLTEXIMAGE2DPROC glTexImage2D{nullptr};
  PFNGLBINDIMAGETEXTUREPROC glBindImageTexture{nullptr};
  PFNGLGETINTEGERI_VPROC glGetIntegeri_v{nullptr};
  PFNGLGETINTEGERVPROC glGetIntegerv{nullptr};
  PFNGLGETSTRINGPROC glGetString{nullptr};
  PFNGLGETSTRINGIPROC glGetStringi{nullptr};
  PFNGLDISPATCHCOMPUTEPROC glDispatchCompute{nullptr};
  PFNGLMEMORYBARRIERPROC glMemoryBarrier{nullptr};
  PFNGLUSEPROGRAMPROC glUseProgram{nullptr};
  PFNGLUNIFORM1IPROC glUniform1i{nullptr};
  PFNGLUNIFORM1FPROC glUniform1f{nullptr};
  PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation{nullptr};
  PFNGLDELETEPROGRAMPROC glDeleteProgram{nullptr};
  PFNGLGETERRORPROC glGetError{nullptr};
  PFNGLGENVERTEXARRAYSPROC glGenVertexArrays{nullptr};
  PFNGLBINDVERTEXARRAYPROC glBindVertexArray{nullptr};
  
  RenderShaderProgInfo _rdrProgInfo;  // 32B
  ComputeShaderProgInfo _cmpProgInfo; // 28B
  // -> 312B
};
}
#endif
