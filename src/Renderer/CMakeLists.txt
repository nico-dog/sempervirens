list(APPEND CMAKE_MESSAGE_CONTEXT renderer)
message(STATUS "Configuring Renderer ...")
target_sources(${CMAKE_PROJECT_NAME}
	PRIVATE
	$<$<AND:$<PLATFORM_ID:Linux>,$<${USE_RENDER_OPENGL}:1>>:${CMAKE_CURRENT_LIST_DIR}/Platform/Linux/Backend/GLRenderer.cpp>)


