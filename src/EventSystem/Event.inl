#include <Logging/Logging.hpp>

namespace sempervirens::eventsystem {




  
  
template<typename T>
EventListener createListener(T* obj) {
  EventListener d;
  d.Bind<T, &T::onEvent>(obj);
  return d;
}

template<typename Event>
void addListener(EventListener listener) {
  assert(Event::_nListeners < nMaxListeners);
  for (auto i = 0; i < Event::_nListeners; ++i) {
    if (listener == Event::_listeners[i]) {
      SEMPERVIRENS_WRN("Listener already exists.");
      return;
    }
  }
  Event::_listeners[Event::_nListeners++] = listener;
}

template<typename Event>
void removeListener(EventListener listener) {
  for (auto i = 0; i < Event::_nListeners; ++i) {
    if (listener == Event::_listeners[i]) {
      std::swap(listener, Event::_listeners[Event::_nListeners - 1]);
    }
  }
  --Event::_nListeners;
}

template<typename Event>
void signal(Event& event) {
  for (auto i = 0; i < Event::_nListeners; ++i)
    Event::_listeners[i].Invoke(event);
}


template<typename T, typename Event>
void _addListener(T* obj) {
  if constexpr (std::is_same_v<Event, WindowCloseEvent>) {
    WindowCloseEventListener d;
    d.Bind<T, &T::onWindowCloseEvent>(obj);
    addWindowCloseEventListener(d);
  }
}

template<typename Event>
void _signal(Event const& event) {
  if constexpr (std::is_same_v<Event, WindowCloseEvent>) {
    signalWindowCloseEvent(event);
  }
}

}
