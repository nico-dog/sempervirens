#define EVENT_CPP
#include <EventSystem/Event.hpp>

namespace sempervirens::eventsystem {

  //static int nMaxListeners{10};  
static WindowCloseEventListener WindowCloseEventListeners[10];

void addWindowCloseEventListener(WindowCloseEventListener listener) {
  WindowCloseEventListeners[0] = listener;
}

void signalWindowCloseEvent(WindowCloseEvent const& event) {
  WindowCloseEventListeners[0].Invoke(event);
}

  
  //static WindowCloseEventListener* head_WindowCloseEventListener = nullptr;
  //static WindowCloseEventListener* tail_WindowCloseEventListener = nullptr;

EventListener WindowCloseEvent::_listeners[nMaxListeners];
int WindowCloseEvent::_nListeners{0};

EventListener WindowExposeEvent::_listeners[nMaxListeners];
int WindowExposeEvent::_nListeners{0};
WindowExposeEvent::WindowExposeEvent(int width, int height)
  : _width{width}, _height{height} {}
  
EventListener WindowResizeEvent::_listeners[nMaxListeners];
int WindowResizeEvent::_nListeners{0};
WindowResizeEvent::WindowResizeEvent(int width, int height)
  : _width{width}, _height{height} {}

EventListener WindowMoveEvent::_listeners[nMaxListeners];
int WindowMoveEvent::_nListeners{0};
WindowMoveEvent::WindowMoveEvent(int xPos, int yPos) : _xPos{xPos}, _yPos{yPos} {}

EventListener WindowFocusInEvent::_listeners[nMaxListeners];
int WindowFocusInEvent::_nListeners{0};
EventListener WindowFocusOutEvent::_listeners[nMaxListeners];
int WindowFocusOutEvent::_nListeners{0};

EventListener KeyPressedEvent::_listeners[nMaxListeners];
int KeyPressedEvent::_nListeners{0};
//KeyPressedEvent::KeyPressedEvent(Keysym_t symbol, Keychr_t chr, Keymod_t mod)  
KeyPressedEvent::KeyPressedEvent(Keysym_t symbol, Keychr_t chr)
  : _symbol{symbol}, _chr{chr} {}

EventListener KeyReleasedEvent::_listeners[nMaxListeners];
int KeyReleasedEvent::_nListeners{0};
KeyReleasedEvent::KeyReleasedEvent(Keysym_t symbol, Keychr_t chr)
  : _symbol{symbol}, _chr{chr} {}

EventListener MouseMovedEvent::_listeners[nMaxListeners];
int MouseMovedEvent::_nListeners{0};
MouseMovedEvent::MouseMovedEvent(int xPos, int yPos, Button_t button)
    : _xPos{xPos}, _yPos{yPos}, _button{button} {}

EventListener MouseButtonPressedEvent::_listeners[nMaxListeners];
int MouseButtonPressedEvent::_nListeners{0};
MouseButtonPressedEvent::MouseButtonPressedEvent(int xPos, int yPos, Button_t button)
  : _xPos{xPos}, _yPos{yPos}, _button{button} {}

EventListener MouseButtonReleasedEvent::_listeners[nMaxListeners];
int MouseButtonReleasedEvent::_nListeners{0};
MouseButtonReleasedEvent::MouseButtonReleasedEvent(int xPos, int yPos, Button_t button)
  : _xPos{xPos}, _yPos{yPos}, _button{button} {}
  
} 
