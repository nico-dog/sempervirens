#ifndef EVENT_HPP
#define EVENT_HPP
#include <Keyboard/KeySymbols.hpp>
#include <Mouse/MouseSymbols.hpp>
using namespace sempervirens::device::keyboard;
using namespace sempervirens::device::mouse;

namespace sempervirens::eventsystem {
// Delegate implementation from Stefan Reinhalter presented in
// Game Engine Gems 3: http://www.gameenginegems.net/geg3.php
// Added comparison operators so we can add/remove listeners from lists
template<typename T>
class Delegate {};

template<typename R, typename... Args>
class Delegate<R(Args...)> {
public:
  Delegate() : _instance(nullptr), _proxy(nullptr) {}

  template<R (*Function)(Args...)>
  void Bind(void) {
    _instance = nullptr;
    _proxy = &FunctionProxy<Function>;
  }

  template<class C, R (C::*Function)(Args...)>
  void Bind(C* instance) {
    _instance = instance;
    _proxy = &MethodProxy<C, Function>;
  }

  template<class C, R (C::*Function)(Args...) const>
  void Bind(const C* instance) {
    _instance = const_cast<C*>(instance);
    _proxy = &ConstMethodProxy<C, Function>;
  }

  R Invoke(Args... args) const {
    assert((_proxy != nullptr) &&
           "Cannot invoke unbound Delegate. Call Bind() first.");
    return _proxy(_instance, std::forward<Args>(args)...);
  }

  inline bool operator==(Delegate<R(Args...)> const& rhs) {
    return _instance == rhs._instance && _proxy == rhs._proxy;
  }
  inline bool operator!=(Delegate<R(Args...)> const& rhs) {
    return _instance != rhs._instance && _proxy != rhs._proxy;
  }

private:
  using Proxy = R (*)(void*, Args...);

  template<R (*Function)(Args...)>
  static inline R FunctionProxy(void*, Args... args) {
    return Function(std::forward<Args>(args)...);
  }

  template<class C, R (C::*Function)(Args...)>
  static inline R MethodProxy(void* instance, Args... args) {
    return (static_cast<C*>(instance)->*Function)(std::forward<Args>(args)...);
  }

  template<class C, R (C::*Function)(Args...) const>
  static inline R ConstMethodProxy(void* instance, Args... args) {
    return (static_cast<const C*>(instance)->*Function)(
        std::forward<Args>(args)...);
  }
  
  void* _instance;
  Proxy _proxy;
};

// Sempervirens event types
enum class EventType {
  WindowClosed,
  WindowExposed,
  WindowResized,
  WindowMoved,
  WindowFocusedIn,
  WindowFocusedOut,
  KeyPressed,
  KeyReleased,
  MouseMoved,
  MouseButtonPressed,
  MouseButtonReleased
};

  
// Event base class for type erasure
class Event {
public:
  virtual ~Event() = default;
  virtual EventType type() const = 0;
};

// Define event listener
using EventListener = Delegate<void(Event&)>;
constexpr int nMaxListeners{5};
  

// Window events
class WindowCloseEvent : public Event {
public:
  WindowCloseEvent() = default;
  ~WindowCloseEvent() = default;
  inline EventType type() const override { return EventType::WindowClosed; }
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

class WindowExposeEvent : public Event {
public:
  WindowExposeEvent(int width, int height);
  ~WindowExposeEvent() = default;
  inline EventType type() const override { return EventType::WindowExposed; }
  int _width;
  int _height;
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

class WindowResizeEvent : public Event {
public:
  WindowResizeEvent(int width, int height);
  ~WindowResizeEvent() = default;
  inline EventType type() const override { return EventType::WindowResized; }
  int _width;
  int _height;
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

class WindowMoveEvent : public Event {
public:
  WindowMoveEvent(int xPos, int yPos);
  ~WindowMoveEvent() = default;
  inline EventType type() const override { return EventType::WindowMoved; }
  int _xPos;
  int _yPos;
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

class WindowFocusInEvent : public Event {
public:
  WindowFocusInEvent() = default;
  ~WindowFocusInEvent() = default;
  inline EventType type() const override { return EventType::WindowFocusedIn; }
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

class WindowFocusOutEvent : public Event {
public:
  WindowFocusOutEvent() = default;
  ~WindowFocusOutEvent() = default;
  inline EventType type() const override { return EventType::WindowFocusedOut; }
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

// Keyboard events
class KeyPressedEvent : public Event {
public:
  //KeyPressedEvent(Keysym_t symbol, Keychr_t chr, Keymod_t mod);
  KeyPressedEvent(Keysym_t symbol, Keychr_t chr);
  ~KeyPressedEvent() = default;
  inline EventType type() const override { return EventType::KeyPressed; }
  Keysym_t _symbol;
  Keychr_t _chr;
  //Keymod_t _mod;
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

class KeyReleasedEvent : public Event {
public:
  KeyReleasedEvent(Keysym_t symbol, Keychr_t chr);
  ~KeyReleasedEvent() = default;
  inline EventType type() const override { return EventType::KeyReleased; }
  Keysym_t _symbol;
  Keychr_t _chr;
  //Keymod_t _mod;
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

// Mouse events
class MouseMovedEvent : public Event {
public:
  MouseMovedEvent(int xPos, int yPos, Button_t button);
  ~MouseMovedEvent() = default;
  inline EventType type() const override { return EventType::MouseMoved; }
  int _xPos;
  int _yPos;
  Button_t _button;
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

class MouseButtonPressedEvent : public Event {
public:
  MouseButtonPressedEvent(int xPos, int yPos, Button_t button);//Buttonmod_t mod);
  ~MouseButtonPressedEvent() = default;
  inline EventType type() const override { return EventType::MouseButtonPressed; }
  int _xPos;
  int _yPos;
  //Buttonmod_t _mod;
  Button_t _button;
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};

class MouseButtonReleasedEvent : public Event {
public:
  MouseButtonReleasedEvent(int xPos, int yPos, Button_t button);
  ~MouseButtonReleasedEvent() = default;
  inline EventType type() const override { return EventType::MouseButtonReleased; }
  int _xPos;
  int _yPos;
  //Buttonmod_t _mod;
  Button_t _button;
  static int _nListeners;
  static EventListener _listeners[nMaxListeners];
};


using WindowCloseEventListener = Delegate<void(WindowCloseEvent const&)>;

void addWindowCloseEventListener(WindowCloseEventListener listener);
void signalWindowCloseEvent(WindowCloseEvent const& event);
  
// helper functions
template<typename T> EventListener createListener(T* obj);
template<typename Event> void addListener(EventListener listener);
template<typename Event> void removeListener(EventListener listener);
template<typename Event> void signal(Event& event);

template<typename T, typename Event> void _addListener(T* obj);
template<typename Event> void _signal(Event& event);
  
// Event system macros
#define SEMPERVIRENS_LISTEN(obj, event)				\
  (sempervirens::eventsystem::addListener<event>(createListener(obj)))
#define SEMPERVIRENS_IGNORE(obj, event)				\
  (sempervirens::eventsystem::removeListener<event>(createListener(obj)))
//#define SEMPERVIRENS_SIGNAL(event) sempervirens::eventsystem::signal(event);


#define SEMPERVIRENS_ADD_LISTENER(obj, event)                              \
  (sempervirens::eventsystem::_addListener<event>(obj))
#define SEMPERVIRENS_REMOVE_LISTENER()
#define SEMPERVIRENS_SIGNAL(event) sempervirens::eventsystem::_signal(event);
}

#include <EventSystem/Event.inl>
#endif
