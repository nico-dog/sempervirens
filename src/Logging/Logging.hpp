#ifndef LOGGING_HPP
#define LOGGING_HPP
#include <Macros/Build.hpp>

namespace sempervirens::logging {
enum class LogSeverity {
  MSG, 
  WRN, 
  ERR, 
  FAT  
};

struct LogInfo {
  char const* file;
  char const* func;
  int line;
};

class Log {
public:
  virtual ~Log() {}
  virtual void log(LogSeverity severity, LogInfo const& info, char const* buffer) {};

protected:
  Log() = default;
  static void AddToList(Log* log);
  static void RemoveFromList(Log* log);

private:
  Log* _next{nullptr};

friend void dispatch(LogSeverity severity, LogInfo const& info, char const* format, ...);  
};

void dispatch(LogSeverity severity, LogInfo const& info, char const* format, ...);

class ConsoleLog : public Log {
public:
  ConsoleLog();
  ~ConsoleLog();

  ConsoleLog(ConsoleLog const&) = delete;
  ConsoleLog& operator=(ConsoleLog const&) = delete;

  void log(LogSeverity severity, LogInfo const& info, const char* buffer) override;

#if SEMPERVIRENS_BUILD(UNITTESTING)
  void static Test();
#endif  
};
  
#if SEMPERVIRENS_BUILD(DEBUG)
#define __FILENAME__							\
  (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define SEMPERVIRENS_LOG(severity, msg, ...)				\
  (sempervirens::logging::dispatch(					\
   sempervirens::logging::LogSeverity::severity, \
   sempervirens::logging::LogInfo{__FILENAME__, __PRETTY_FUNCTION__, __LINE__}, \
   msg, ##__VA_ARGS__))  
#else    
#define SEMPERVIRENS_LOG(severity, msg, ...)	\
  do {} while (0)
#endif
#define SEMPERVIRENS_MSG(msg, ...) SEMPERVIRENS_LOG(MSG, msg, ##__VA_ARGS__)
#define SEMPERVIRENS_WRN(msg, ...) SEMPERVIRENS_LOG(WRN, msg, ##__VA_ARGS__)
#define SEMPERVIRENS_ERR(msg, ...) SEMPERVIRENS_LOG(ERR, msg, ##__VA_ARGS__)
#define SEMPERVIRENS_FAT(msg, ...) SEMPERVIRENS_LOG(FAT, msg, ##__VA_ARGS__)
}
#endif

