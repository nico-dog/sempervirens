#define LOGGING_CPP
#include <Logging/Logging.hpp>
#include <cstdarg>

namespace sempervirens::logging {

static Log* head_log = nullptr;
static Log* tail_log = nullptr;
static std::mutex mutex_log{};
  
void Log::AddToList(Log* log) {
  std::lock_guard<std::mutex> lock(mutex_log);
  if (head_log) {
    tail_log->_next = log;
    tail_log = log;
  } else {
    head_log = log;
    tail_log = log;
  }
}

void Log::RemoveFromList(Log* log) {
  std::lock_guard<std::mutex> lock(mutex_log);
  if (log == head_log) {
    if (log == tail_log) {
      head_log = nullptr;
      tail_log = nullptr;
    } else {
      head_log = head_log->_next;
    }
  }
  else {
    auto prev = head_log;
    while (prev->_next != log) {
      prev = prev->_next;
    }
    if (log == tail_log) {
      prev->_next = nullptr;
      tail_log = prev;
    } else {
      prev->_next = log->_next;
    }
  }
}

void dispatch(LogSeverity severity, LogInfo const& info, const char* format, ...) {
  char buffer[512];
  va_list args;
  va_start(args, format);
  vsprintf(buffer, format, args);
  va_end(args);

  std::lock_guard<std::mutex> lock(mutex_log);
  auto log = head_log;
  do {
    log->log(severity, info, buffer);
    log = log->_next;
  } while (log);
}

ConsoleLog::ConsoleLog() { AddToList(this); }
ConsoleLog::~ConsoleLog() { RemoveFromList(this); }

void ConsoleLog::log(LogSeverity severity, LogInfo const& info, const char* buffer) {
  char const* prefix = nullptr;
  switch (severity) {
  case LogSeverity::MSG:
    prefix = "[MSG]";
    break;
  case LogSeverity::WRN:
    prefix = "[WRN]";
    break;
  case LogSeverity::ERR:
    prefix = "[ERR]";
    break;
  case LogSeverity::FAT:
    prefix = "[FAT]";
    break;
  default:
    prefix = "";
  }

  printf("%s %s, %s, %d: %s", prefix, info.file, info.func, info.line, buffer);
}
  
#if SEMPERVIRENS_BUILD(UNITTESTING)
#include <thread>
void ConsoleLog::Test() {
  auto thread1 = std::thread([](){ auto log = ConsoleLog{}; for (auto i = 0; i < 2; ++i) SEMPERVIRENS_MSG("*** From Thread %d ***\n", 1); });
  auto thread2 = std::thread([](){ auto log = ConsoleLog{}; for (auto i = 0; i < 2; ++i) SEMPERVIRENS_MSG("*** From Thread %d ***\n", 2); });
  for (auto i = 0; i < 10; ++i)
    SEMPERVIRENS_MSG("*** From Main Thread ***\n");
  thread1.join();
  thread2.join();
}
#endif  
} 
