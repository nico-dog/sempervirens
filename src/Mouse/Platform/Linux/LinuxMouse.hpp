#ifndef LINUXMOUSE_HPP
#define LINUXMOUSE_HPP
#include <EventSystem/Event.hpp>
#include <Mouse/MouseSymbols.hpp>

namespace sempervirens::device::mouse {
class LinuxMouse {
public:
  LinuxMouse();
  ~LinuxMouse();

  void onEvent(sempervirens::eventsystem::Event& event);
  bool wentDown(Button_t button);
  bool isPressed(Button_t button);
  bool wentUp(Button_t button);

private:
  int _xPos{0};                         // 4B
  int _yPos{0};                         // 4B
  Button_t _down{Button_t::Void};
  Button_t _pressed{Button_t::Void};
  Button_t _up{Button_t::Void};
};
} 
#endif
