#define LINUXMOUSE_CPP
#include <Logging/Logging.hpp>
#include <Mouse/Platform/Linux/LinuxMouse.hpp>

using namespace sempervirens::eventsystem;

namespace sempervirens::device::mouse {
LinuxMouse::LinuxMouse() {
  SEMPERVIRENS_MSG("LinuxMouse ctor, size of LinuxMouse: %d\n", sizeof(LinuxMouse));
  SEMPERVIRENS_LISTEN(this, MouseMovedEvent);
  SEMPERVIRENS_LISTEN(this, MouseButtonPressedEvent);
  SEMPERVIRENS_LISTEN(this, MouseButtonReleasedEvent);
}

LinuxMouse::~LinuxMouse() { SEMPERVIRENS_MSG("LinuxMouse dtor\n"); }

void LinuxMouse::onEvent(Event& event) {
  if (event.type() == EventType::MouseMoved) {
    auto& e = static_cast<MouseMovedEvent&>(event);
    _up = Button_t::Void;

    //SEMPERVIRENS_MSG("Mouse moved: button %s\n", to_string(e._button));

    if (e._button == _down) {
      _pressed = _down;
      _down = Button_t::Void;
    }
    
    _xPos = e._xPos;
    _yPos = e._yPos;
    return;
  }

  if (event.type() == EventType::MouseButtonPressed) {
    auto& e = static_cast<MouseButtonPressedEvent&>(event);
    _up = Button_t::Void;
    (_down == e._button) ? _pressed = e._button : _down = e._button;
    if (_pressed != Button_t::Void) _down = Button_t::Void;
    _xPos = e._xPos;
    _yPos = e._yPos;

    SEMPERVIRENS_MSG("Pressed button: up %s, down %s, pressed %s, mod %s\n",
		     to_string(_up), to_string(_down), to_string(_pressed));
    //to_string(e._mod));
    return;
  }

  if (event.type() == EventType::MouseButtonReleased) {
    auto& e = static_cast<MouseButtonReleasedEvent&>(event);
    _down = Button_t::Void;
    _pressed = Button_t::Void;
    _up = e._button;
    _xPos = e._xPos;
    _yPos = e._yPos;

    SEMPERVIRENS_MSG("Released button: up %s, down %s, pressed %s, mod %s\n",
		     to_string(_up), to_string(_down), to_string(_pressed));
    //to_string(e._mod));
    return;
  }
}

bool LinuxMouse::wentDown(Button_t button) {
  return button == _down && _pressed == Button_t::Void;
}

bool LinuxMouse::isPressed(Button_t button) {
  return button == _pressed;
}

bool LinuxMouse::wentUp(Button_t button) {
  return button == _up;
}
} 
