#define MOUSESYMBOLS_CPP
#include <Mouse/MouseSymbols.hpp>

namespace sempervirens::device::mouse {
char const* to_string(Button_t button) {
  if (button == Button_t::Left) return "Left";
  else if (button == Button_t::Right) return "Right";
  else if (button == Button_t::Scroll) return "Scroll";
  else if (button == Button_t::ScrollDown) return "Scroll Down";
  else if (button == Button_t::ScrollUp) return "Scroll Up";
  return "Void";
}

char const* to_string(Buttonmod_t mod) {
  if (mod & LEFT())     return "+LEFT";
  else if (mod & RIGHT()) return "+RIGHT";
  else if (mod & SCROLL()) return "+SCROLL";
  return "NONE";
}
}
