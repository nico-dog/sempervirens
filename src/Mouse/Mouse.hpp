#ifndef MOUSE_HPP
#define MOUSE_HPP
#include <Macros/Platform.hpp>

#if SEMPERVIRENS_PLATFORM(UNIX)
#include <Mouse/Platform/Linux/LinuxMouse.hpp>

using Mouse_t = sempervirens::device::mouse::LinuxMouse;
#endif

#endif
