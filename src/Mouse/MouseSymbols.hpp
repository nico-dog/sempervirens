#ifndef MOUSESYMBOLS_HPP
#define MOUSESYMBOLS_HPP
#include <Macros/Platform.hpp>

namespace sempervirens::device::mouse {
enum class Button_t : unsigned int {
  Void,
  Left,
  Scroll,
  Right,
  ScrollUp,
  ScrollDown
};

using Buttonmod_t = std::int8_t;
  
// State modifiers
#define NONE()   0 
#define LEFT()   1 << 0  // remapped Xlib Button1Mask (1 << 8)
#define RIGHT()  1 << 1  // remapped Xlib Button2Mask (1 << 9)
#define SCROLL() 1 << 2  // remapped Xlib Button3Mask (1 << 10)

char const* to_string(Button_t);
char const* to_string(Buttonmod_t);
}

//char const* to_string(sempervirens::device::mouse::Button_t);
//char const* to_string(sempervirens::device::mouse::Buttonmod_t);
#endif
