list(APPEND CMAKE_MESSAGE_CONTEXT window)
message(STATUS "Configuring Window ...")
target_sources(${CMAKE_PROJECT_NAME}
	PRIVATE
	$<$<PLATFORM_ID:Linux>:${CMAKE_CURRENT_LIST_DIR}/Platform/Linux/Backend/XGLWindow.cpp>)

