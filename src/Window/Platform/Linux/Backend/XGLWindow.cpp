#define XGLWINDOW_CPP
#include <EventSystem/Event.hpp>
#include <Window/Platform/Linux/Backend/XGLWindow.hpp>
#include <X11/XKBlib.h>
#include <GL/glxext.h>

using namespace sempervirens::eventsystem;

namespace sempervirens::window {
XGLWindow::XGLWindow() {
  SEMPERVIRENS_MSG("XGLWindow ctor, size of XGLWindow: %d\n", sizeof(XGLWindow));
  if (initDisplay()) {
    initWindow();  
  }
}

XGLWindow::~XGLWindow() {
  SEMPERVIRENS_MSG("XGLWindow dtor\n");
  closeWindow();
}

bool XGLWindow::initDisplay() {
  // Connect to the display of the machine the client code runs on (local display)
  _display = XOpenDisplay(getenv("DISPLAY"));
  if (!_display) {
    SEMPERVIRENS_FAT("Cannot connect to X server\n");
    return false;
  }
  
  // Fill in screen information
  _screen._id = DefaultScreen(_display);
  _screen._width = DisplayWidth(_display, _screen._id);
  _screen._height = DisplayHeight(_display, _screen._id);
  _screen._rootWindow = RootWindow(_display, _screen._id);
  SEMPERVIRENS_MSG("Default screen info:\nID: %d\nwidth: %d\nheight: %d\n",
		   _screen._id, _screen._width, _screen._height);

  // Enable detectable auto repeat for key pressed events
  // Prevents the generation of a key released event for each key pressed event
  // when a key is held down
  auto detectableAutoRepeatIsEnabled =
      XkbSetDetectableAutoRepeat(_display, true, nullptr);
  if (detectableAutoRepeatIsEnabled)
    SEMPERVIRENS_MSG("Detectable auto repeat is enabled\n");
  // Note: if not supported we could go around by using XQueryKeymap that
  // returns an array of 32 bytes where each bit is set if the corresponding key
  // is held down.
  return true;
}

bool XGLWindow::initWindow() {
  _window = XCreateSimpleWindow(_display, _screen._rootWindow,
				0, 0,     // x, y 
				800, 600, // width, height 
				0, 0,     // border width, border 
				0);       // background 
  XStoreName(_display, _window, "Sempervirens window");
  
  // Customize event mask (override parent window attributes)
  XSetWindowAttributes windowAttr;
  windowAttr.event_mask =
    ExposureMask |        // For exposure events
    StructureNotifyMask | // For resize and move events
    FocusChangeMask |     // For focus in and out events
    KeyPressMask |        // For key press events
    KeyReleaseMask |      // For key release events
    ButtonPressMask |     // For mouse button press events
    ButtonReleaseMask |   // For mouse button release events
    PointerMotionMask;    // For mouse motion events, irrespective of button states  
  XChangeWindowAttributes(_display, _window, CWEventMask, &windowAttr);
    
  // Intercept window close event from window manager
  Atom wmDeleteMessage = XInternAtom(_display, "WM_DELETE_WINDOW", false);
  XSetWMProtocols(_display, _window, &wmDeleteMessage, 1);

  // Find frame buffer config matching visual attributes
  static int visual_attr[] = {
    GLX_RENDER_TYPE, GLX_RGBA_BIT,
    GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
    GLX_DOUBLEBUFFER, true,
    GLX_RED_SIZE, 1,
    GLX_GREEN_SIZE, 1,
    GLX_BLUE_SIZE, 1,
    None
  };
  int fbcID = 0;
  GLXFBConfig* fbc = glXChooseFBConfig(_display,_screen._id, visual_attr, &fbcID);
  if (!fbc) {
    SEMPERVIRENS_FAT("glXChooseFBConfig() failed\n");
    return false;
  }
  
  // Create modern (4.3) OpenGL context
  auto glXExtensions = glXQueryExtensionsString(_display, _screen._id);
  if (strstr(glXExtensions, "GLX_ARB_create_context")) {
    SEMPERVIRENS_MSG("GLX context attributes extension is available\n");
    PFNGLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribsARB = nullptr;
    glXCreateContextAttribsARB = (PFNGLXCREATECONTEXTATTRIBSARBPROC)glXGetProcAddressARB((const GLubyte*)"glXCreateContextAttribsARB");
    // Set desired minimum OpenGL version 
    static int context_attr[] = {
      GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
      GLX_CONTEXT_MINOR_VERSION_ARB, 3,
      None
    };
    _glc = glXCreateContextAttribsARB(_display, fbc[0],
				      nullptr, // not sharing with other contexts
				      true,    // direct rendering
				      context_attr);
    if (!_glc) {
      SEMPERVIRENS_FAT("Failed to create OpenGL context\n");
      return false;
    }
  }
  else {
    SEMPERVIRENS_FAT("GLX context attributes extension is NOT available\n");
    return false;
  }

  // Bind openGL context to the window
  XMapWindow(_display, _window);
  glXMakeCurrent(_display, _window, _glc);

  // If GLX extension for VSync is available, enable Adaptive VSync
  if (strstr(glXExtensions, "GLX_EXT_swap_control")) {
    SEMPERVIRENS_MSG("GLX swap control extension available\n");
    PFNGLXSWAPINTERVALEXTPROC glXSwapIntervalEXT = nullptr;
    glXSwapIntervalEXT = (PFNGLXSWAPINTERVALEXTPROC)glXGetProcAddressARB((const GLubyte*)"glXSwapIntervalEXT");
    GLXDrawable drawable = glXGetCurrentDrawable();
    unsigned int swap = 0;
    unsigned int maxSwap = 0;
    glXQueryDrawable(_display, drawable, GLX_SWAP_INTERVAL_EXT, &swap);
    glXQueryDrawable(_display, drawable, GLX_MAX_SWAP_INTERVAL_EXT, &maxSwap);
    SEMPERVIRENS_MSG("The swap interval is %u and the max swap interval is %u\n", swap, maxSwap);
    
    glXSwapIntervalEXT(_display, drawable, -1); //1: VSync enabled, 0: disabled, -1: adpative
  }
  else {
    SEMPERVIRENS_MSG("GLX swap control extension is NOT available\n");
  }
  return true;
}

void XGLWindow::closeWindow() {
  glXMakeCurrent(_display, None, nullptr);
  glXDestroyContext(_display, _glc);
  XDestroyWindow(_display, _window);
  XCloseDisplay(_display);
}

bool XGLWindow::processInput() {
  XEvent xevent;
  // If the event queue is empty, XNextEvent blocks till the next event is
  // received. Avoid blocking by checking if the queue contains events before
  // calling XNextEvent.
  if (XPending(_display)) { // Equivalent to XEventsQueued(_display, QueuedAfterFlush)
    XNextEvent(_display, &xevent);
  }
  else {
    return false;
  }
  
  switch (xevent.type) {
  case ClientMessage: {
    Atom wmDeleteMessage = XInternAtom(_display, "WM_DELETE_WINDOW", false);
    auto client = xevent.xclient;
    if (client.data.l[0] == static_cast<long int>(wmDeleteMessage)) {
      auto event = WindowCloseEvent{};
      SEMPERVIRENS_SIGNAL(event);
    }
    break;
  }

  case Expose: {
    XWindowAttributes gwa;
    XGetWindowAttributes(_display, _window, &gwa);
    _width = gwa.width;
    _height = gwa.height;    
    auto event = WindowExposeEvent{_width, _height};
    SEMPERVIRENS_SIGNAL(event);
    break;
  }

  case ConfigureNotify: {
    auto config = xevent.xconfigure;
    if (config.width != _width || config.height != _height) {
      _width = config.width;
      _height = config.height;
      auto event = WindowResizeEvent{_width, _height};
      SEMPERVIRENS_SIGNAL(event);
      break;
    }
    if (config.x != _xPos || config.y != _yPos) {
      // auto event WindowMoveEvent{config.x, config.y};
      // SEMPERVIRENS_SIGNAL(event);
      _xPos = config.x;
      _yPos = config.y;
      break;
    }
  }

  case FocusIn: {
    // WindowFocusInEvent focusInEvent{};
    // SEMPERVIRENS_SIGNAL(focusInEvent);
    break;
  }

  case FocusOut: {
    // auto event = WindowFocusOutEvent{};
    // SEMPERVIRENS_SIGNAL(event);
    break;
  }

  // Mouse events
  case MotionNotify: {
    auto motion = xevent.xmotion;
    auto button = Button_t::Void;
    if (motion.state & Button1Mask) button = Button_t::Left;
    else if (motion.state & Button2Mask) button = Button_t::Scroll;
    else if (motion.state & Button3Mask) button = Button_t::Right;
    auto event = MouseMovedEvent{motion.x, motion.y, button};
    SEMPERVIRENS_SIGNAL(event);
    break;
  }

  case ButtonPress:
    [[fallthrough]]; // Can distinguish types for press (=4) and release (=5) button events
  case ButtonRelease: {
    auto buttonEvent = xevent.xbutton;

    //ButtonState_t state = buttonEvent.state & Button1Mask ? LEFT() : NONE();
    //state |= buttonEvent.state & Button3Mask ? RIGHT() : NONE();
    //state |= buttonEvent.state & Button2Mask ? SCROLL() : NONE();
    
    if (buttonEvent.type == 5) {
      auto event = MouseButtonReleasedEvent{buttonEvent.x, buttonEvent.y, //mod,
					    static_cast<Button_t>(buttonEvent.button)};
      SEMPERVIRENS_SIGNAL(event);
    }
    else {
      auto event = MouseButtonPressedEvent{buttonEvent.x, buttonEvent.y, //mod,
					   static_cast<Button_t>(buttonEvent.button)};
      SEMPERVIRENS_SIGNAL(event);
    }
    break;
  }

  // Keyboard events
  case KeyPress: 
    [[fallthrough]]; // Can distinguish types for press (=2) and release (=3) key events
  case KeyRelease: {
    auto keyEvent = xevent.xkey;
    auto symbol = XkbKeycodeToKeysym(
        _display, keyEvent.keycode, 0,
        keyEvent.state & ShiftMask || keyEvent.state & LockMask ? 1 : 0);
    if (symbol == NoSymbol) {
      SEMPERVIRENS_ERR("Key has no symbol");
      break;
    }

    auto chr = XKeysymToString(symbol);
    //Keymod_t mod = keyEvent.state & ShiftMask ? SHIFT() : NONE();
    //mod |= keyEvent.state & LockMask ? LOCK() : NONE();
    //mod |= keyEvent.state & ControlMask ? CTRL() : NONE();
    //mod |= keyEvent.state & Mod1Mask ? ALT() : NONE();
    //mod |= keyEvent.state & Mod2Mask ? NUML() : NONE();
    if (keyEvent.type == 3) {
      //auto event = KeyReleasedEvent{symbol, chr, mod};
      auto event = KeyReleasedEvent{symbol, chr};
      SEMPERVIRENS_SIGNAL(event);
    }
    else {
      auto event = KeyPressedEvent{symbol, chr};      
      SEMPERVIRENS_SIGNAL(event);
    }
    break;
  }

  default:
    break;
  }

  //}
  // SEMPERVIRENS_MSG("No window event");
  return true;
}

void XGLWindow::update() {
  glXSwapBuffers(_display, _window);     
}

#if SEMPERVIRENS_BUILD(UNITTESTING)
void XGLWindow::Test() {
  auto window = XGLWindow();
  // auto windowInfo = WindowInfo{"Test Window", 0, 0, 1.0f / 2, 2.0f / 3};
  // auto window = static_cast<XWindow*>(createWindow(windowInfo));
  // delete window;
}
#endif
} 
