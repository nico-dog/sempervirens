#define XWWINDOW_CPP
#include <EventSystem/Event.hpp>
#include <Window/Platform/Linux/Backend/XWindow.hpp>
#include <Config/ConfigSetting.hpp>
#include <X11/XKBlib.h>

namespace sempervirens::config {
extern ConfigSettingType<int> g_Verbosity;
}

using namespace sempervirens::eventsystem;

namespace sempervirens::window {
// XWindow::XWindow(WindowInfo const& windowInfo)
XWindow::XWindow() {
  SEMPERVIRENS_MSG("XWindow ctor, size of XWindow: %d\n", sizeof(XWindow));
  // initDisplay(windowInfo);
  initDisplay();
}

XWindow::~XWindow() {
  SEMPERVIRENS_MSG("XWindow dtor\n");
  XCloseDisplay(_display);
}

// bool XWindow::initDisplay(WindowInfo const& windowInfo)
bool XWindow::initDisplay() {
  // Connect to the display of the machine the client code runs on (local
  // display).
  _display = XOpenDisplay(getenv("DISPLAY"));
  if (!_display) {
    SEMPERVIRENS_FAT("Cannot connect to X server");
    return false;
  }

  // Get screen information.
  _screen._id = DefaultScreen(_display);
  _screen._width = DisplayWidth(_display, _screen._id);
  _screen._height = DisplayHeight(_display, _screen._id);
  _screen._rootWindow = RootWindow(_display, _screen._id);
  _screen._whitePixel = WhitePixel(_display, _screen._id);
  _screen._blackPixel = BlackPixel(_display, _screen._id);
  SEMPERVIRENS_MSG("Screen info:\n .. id: %d\n .. width: %d\n .. height: %d\n \
.. root window id: %d\n", _screen._id, _screen._width, _screen._height, _screen._rootWindow);

  // Create the window.
  _window =
      XCreateSimpleWindow(_display, _screen._rootWindow,
                          // windowInfo._xPos, windowInfo._yPos,
                          0, 0,
                          // windowInfo._widthRatio * _screen._width,
                          // windowInfo._heightRatio * _screen._height,
                          _screen._width, _screen._height, 1,
                          _screen._blackPixel,  // 1 pixel wide black border
                          _screen._whitePixel); // white background
  // XStoreName(_display, _window, windowInfo._title.c_str());
  XStoreName(_display, _window, "Sempervirens Appication");
  // auto gc = XCreateGC(_display, _window, 0,0);
  // XSetBackground(_display, gc, _screen._whitePixel);
  // XSetForeground(_display, gc, _screen._blackPixel);

  // Set event masks.
  XSelectInput(_display, _window,
               ExposureMask |            // For exposure events.
                   StructureNotifyMask | // For resize and move events.
                   FocusChangeMask |     // For focus in and out events.
                   KeyPressMask |        // For key press events.
                   KeyReleaseMask |      // For key release events.
                   ButtonPressMask |     // For mouse button press events.
                   ButtonReleaseMask |   // For mouse button release events.
                   PointerMotionMask); // For mouse motion events, irrespective
                                       // of button states.

  // Enable detectable auto repeat for key pressed events.
  auto detectableAutoRepeatIsEnabled =
      XkbSetDetectableAutoRepeat(_display, true, nullptr);
  if (detectableAutoRepeatIsEnabled)
    SEMPERVIRENS_MSG("Detectable auto repeat is enabled.");
  // Note: if not supported we could go around by using XQueryKeymap that
  // returns an array of 32 bytes where each bit is set if the corresponding key
  // is held down.

  // Intercept window close event from window manager.
  Atom wmDeleteMessage = XInternAtom(_display, "WM_DELETE_WINDOW", false);
  XSetWMProtocols(_display, _window, &wmDeleteMessage, 1);

  // Map window to screen.
  XClearWindow(_display, _window);
  XMapWindow(_display, _window);
  XFlush(_display);

  return true;
}

void XWindow::processInput() {
  XEvent xevent;
  // If the event queue is empty, XNextEvent blocks till the next event is
  // received. Avoid blocking by checking if the queue contains events before
  // calling XNextEvent.
  if (XPending(
          _display)) // Equivalent to XEventsQueued(_display, QueuedAfterFlush)
    XNextEvent(_display, &xevent);
  else
    return;

  switch (xevent.type) {
  case ClientMessage: {
    Atom wmDeleteMessage = XInternAtom(_display, "WM_DELETE_WINDOW", false);
    auto client = xevent.xclient;
    if (client.data.l[0] == static_cast<long int>(wmDeleteMessage)) {
      auto event = WindowCloseEvent{};
      SEMPERVIRENS_SIGNAL(event);
    }
    break;
  }

  case Expose: {
    // auto event = WindowExposeEvent{};
    // SEMPERVIRENS_SIGNAL(event);
    break;
  }

  case ConfigureNotify: {
    auto config = xevent.xconfigure;
    if (config.width != _width || config.height != _height) {
      // auto event = WindowResizeEvent{config.width, config.height};
      // SEMPERVIRENS_SIGNAL(event);
      _width = config.width;
      _height = config.height;
      break;
    }
    if (config.x != _xPos || config.y != _yPos) {
      // auto event WindowMoveEvent{config.x, config.y};
      // SEMPERVIRENS_SIGNAL(event);
      _xPos = config.x;
      _yPos = config.y;
      break;
    }
  }

  case FocusIn: {
    // WindowFocusInEvent focusInEvent{};
    // SEMPERVIRENS_SIGNAL(focusInEvent);
    break;
  }

  case FocusOut: {
    // auto event = WindowFocusOutEvent{};
    // SEMPERVIRENS_SIGNAL(event);
    break;
  }

  // Mouse events
  case MotionNotify: {
    auto motion = xevent.xmotion;
    auto event = MouseMovedEvent{motion.x, motion.y};
    SEMPERVIRENS_SIGNAL(event);
    break;
  }

  case ButtonPress: // Fallthrough. Can distinguish types for press (=4) and
                    // release (=5) button events.
  case ButtonRelease: {
    auto buttonEvent = xevent.xbutton;
    if (buttonEvent.type == 5) {
      auto event =
          MouseButtonReleasedEvent{buttonEvent.x, buttonEvent.y,
                                   static_cast<Button_t>(buttonEvent.button)};
      SEMPERVIRENS_SIGNAL(event);
    } else {
      auto event =
          MouseButtonPressedEvent{buttonEvent.x, buttonEvent.y,
                                  static_cast<Button_t>(buttonEvent.button)};
      SEMPERVIRENS_SIGNAL(event);
    }
    break;
    [[fallthrough]];
  }

  // Keyboard events
  case KeyPress: // Fallthrough. Can distinguish types for press (=2) and
                 // release (=3) key events.
  case KeyRelease: {
    auto keyEvent = xevent.xkey;
    auto symbol = XkbKeycodeToKeysym(
        _display, keyEvent.keycode, 0,
        keyEvent.state & ShiftMask || keyEvent.state & LockMask ? 1 : 0);
    if (symbol == NoSymbol) {
      SEMPERVIRENS_ERR("Key has no symbol");
      break;
    }

    auto chr = XKeysymToString(symbol);

    Keymod_t mod = keyEvent.state & ShiftMask ? SHIFT() : NONE();
    mod |= keyEvent.state & LockMask ? LOCK() : NONE();
    mod |= keyEvent.state & ControlMask ? CTRL() : NONE();
    mod |= keyEvent.state & Mod1Mask ? ALT() : NONE();
    mod |= keyEvent.state & Mod2Mask ? NUML() : NONE();

    if (keyEvent.type == 3) {
      auto event = KeyReleasedEvent{symbol, chr, mod};
      SEMPERVIRENS_SIGNAL(event);
    } else {
      if (symbol == KEY_q && mod == CTRL()) {
	auto event = WindowCloseEvent{};
	SEMPERVIRENS_SIGNAL(event);
	break;
      }

      if (symbol == KEY_v && mod == CTRL()) {
	sempervirens::config::g_Verbosity = !sempervirens::config::g_Verbosity;
      }
      
      auto event = KeyPressedEvent{symbol, chr, mod};      
      SEMPERVIRENS_SIGNAL(event);
    }
    break;
    [[fallthrough]];
  }

  default:
    break;
  }

  //}
  // SEMPERVIRENS_MSG("No window event");
  return;
}

// void XWindow::onUpdate()
//{
//  SEMPERVIRENS_MSG("Window update...");
//}

#if SEMPERVIRENS_BUILD(UNITTESTING)
void XWindow::Test() {
  auto window = XWindow();
  // auto windowInfo = WindowInfo{"Test Window", 0, 0, 1.0f / 2, 2.0f / 3};
  // auto window = static_cast<XWindow*>(createWindow(windowInfo));
  // delete window;
}
#endif
} 
