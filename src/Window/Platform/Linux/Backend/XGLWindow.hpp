#ifndef XWINDOW_HPP
#define XWINDOW_HPP
#include <Macros/Build.hpp>
#include <X11/Xlib.h>
#include <GL/glx.h>

namespace sempervirens::window {

struct ScreenInfo {
  int _id;
  int _width;
  int _height;
  Window _rootWindow;
};

class XGLWindow {
public:
  XGLWindow();
  ~XGLWindow();

  bool processInput();
  void update();
#if SEMPERVIRENS_BUILD(UNITTESTING)
  void static Test();
#endif
  
private:
  Display* _display{nullptr}; // 8B
  Window _window;             // 8B
  GLXContext _glc;            // 8B
  int _width{0};              // 4B
  int _height{0};             // 4B
  int _xPos{0};               // 4B
  int _yPos{0};               // 4B
  ScreenInfo _screen;         // 24B

  bool initDisplay();
  bool initWindow();
  void closeWindow();
};

} // namespace sempervirens::window
#endif
