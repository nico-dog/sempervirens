#ifndef XWINDOW_HPP
#define XWINDOW_HPP
#include <Macros/Build.hpp>
#include <GL/glx.h>
#include <X11/Xlib.h>

namespace sempervirens::window {

struct ScreenInfo {
  int _id;
  int _width;
  int _height;
  Window _rootWindow;
  unsigned long _whitePixel;
  unsigned long _blackPixel;
};

class GLXWindow {
public:
  GLXWindow();
  ~GLXWindow();

  void processInput();
#if SEMPERVIRENS_BUILD(UNITTESTING)
  void static Test();
#endif
  
private:
  Display* _display{nullptr}; // 8B
  Window _window;             // 8B
  int _width{0};              // 4B
  int _height{0};             // 4B
  int _xPos{0};               // 4B
  int _yPos{0};               // 4B
  ScreenInfo _screen;         // 40B

  bool initDisplay();
};

} // namespace sempervirens::window
#endif
