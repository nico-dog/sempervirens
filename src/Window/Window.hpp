#ifndef WINDOW_HPP
#define WINDOW_HPP
#include <Macros/Build.hpp>
#include <Macros/Platform.hpp>

#if SEMPERVIRENS_PLATFORM(UNIX)
#if SEMPERVIRENS_BUILD(OPENGL)
#include <Window/Platform/Linux/Backend/XGLWindow.hpp>
using Window_t = sempervirens::window::XGLWindow;
#endif
#endif

#endif
