#ifndef KEYSYMBOLS_HPP
#define KEYSYMBOLS_HPP
#include <Macros/Platform.hpp>
#if SEMPERVIRENS_PLATFORM(UNIX)
#include <Keyboard/Platform/Linux/LinuxKeySymbols.hpp>

namespace sempervirens::device::keyboard {
using Keysym_t = KeySym; // 8B
using Keychr_t = char*;  // 8B
using Keymod_t = std::int8_t;

// State modifiers
#define NONE() 0
#define SHIFT() 1 << 0 // Xlib ShiftMask
#define LOCK()  1 << 1 // Xlib LockMask
#define CTRL()  1 << 2 // Xlib ControlMask
#define ALT()   1 << 3 // Xlib Mod1Mask
#define NUML()  1 << 4 // Xlib Mod2Mask

char const* to_string(Keymod_t);  
} 
#endif

#endif
