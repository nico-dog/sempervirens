#ifndef LINUXKEYBOARD_HPP
#define LINUXKEYBOARD_HPP
#include <EventSystem/Event.hpp>
#include <Keyboard/Platform/Linux/LinuxKeySymbols.hpp>

namespace sempervirens::device::keyboard {
class LinuxKeyboard {
public:
  LinuxKeyboard();
  ~LinuxKeyboard();

  void onEvent(sempervirens::eventsystem::Event& event);
  bool wentDown(Keysym_t key);
  bool isPressed(Keysym_t key);
  bool wentUp(Keysym_t key);

private:
  // Define state to account for chords such as:
  // single key down / pressed / up
  // CRTL/ALT + key down / pressed / up
  // CTRL + ALT + key down / pressed / up
  // CTRL + key1 down + key2 down
  Keysym_t _state1{NONE()}; // 8B
  Keysym_t _state2{NONE()};
  Keysym_t _down{0};
  Keysym_t _down1{0};
  Keysym_t _down2{0};
  Keysym_t _pressed{0};
  Keysym_t _up{0};
  // -> 56B
};
} 
#endif
