#define LINUXKEYBOARD_CPP
#include <Keyboard/Platform/Linux/LinuxKeyboard.hpp>
#include <Logging/Logging.hpp>
using namespace sempervirens::eventsystem;

namespace sempervirens::device::keyboard {
LinuxKeyboard::LinuxKeyboard() {
  SEMPERVIRENS_MSG("LinuxKeyboard ctor, size of LinuxKeyboard: %d\n", sizeof(LinuxKeyboard));
  SEMPERVIRENS_LISTEN(this, KeyPressedEvent);
  SEMPERVIRENS_LISTEN(this, KeyReleasedEvent);
}

LinuxKeyboard::~LinuxKeyboard() { SEMPERVIRENS_MSG("LinuxKeyboard dtor\n"); }

void LinuxKeyboard::onEvent(Event& event) {
  if (event.type() == EventType::KeyPressed) {
    auto& e = static_cast<KeyPressedEvent&>(event);
    // key
    _up = 0;
    (_down == e._symbol) ? _pressed = e._symbol : _down = e._symbol;
    if (_pressed) _down = 0;
    // state + sequence
    if ((e._symbol == KEY_CTRLL) || (e._symbol == KEY_CTRLR)) {
      ~(_state1 & NONE()) ? _state1 = CTRL() : _state2 = CTRL();
    }
    else if ((e._symbol == KEY_ALTL) || (e._symbol == KEY_ALTR)) {
      ~(_state1 & NONE()) ? _state1 = ALT() : _state2 = ALT();
    }
    else if (_state1 & (CTRL() | ALT())) {
      _down1 ? _down2 = e._symbol : _down1 = e._symbol;
    }
    //SEMPERVIRENS_MSG("up %d, down %d, pressed %d\n", _up, _down, _pressed);
    //SEMPERVIRENS_MSG("state1 %s, state2 %s, down1 %d, down2 %d\n", to_string(_state1), to_string(_state2), _down1, _down2);
    return;
  }

  if (event.type() == EventType::KeyReleased) {
    auto& e = static_cast<KeyReleasedEvent&>(event);
    // key
    _down = 0;
    _pressed = 0;
    _up = e._symbol;
    // state + sequence
    if ((e._symbol == KEY_CTRLL) || (e._symbol == KEY_CTRLR)) {
      (_state1 & CTRL()) ? _state1 = NONE() : _state2 = NONE();
      _down1 = 0;
      _down2 = 0;
    }
    else if ((e._symbol == KEY_ALTL) || (e._symbol == KEY_ALTR)) {
      (_state1 & ALT()) ? _state1 = NONE() : _state2 = NONE();
      _down1 = 0;
      _down2 = 0;
    }
    else if ((_state1 & (CTRL() | ALT())) && _down1 && _down2) {
      _down1 = 0;
      _down2 = 0;
    }
    //SEMPERVIRENS_MSG("up %d, down %d, pressed %d\n", _up, _down, _pressed);
    //SEMPERVIRENS_MSG("state1 %s, state2 %s, down1 %d, down2 %d\n", to_string(_state1), to_string(_state2), _down1, _down2);
    return;
  }
}

bool LinuxKeyboard::wentDown(Keysym_t key) {
  if ((_state1 & (CTRL() | ALT())) && _down1 && _down2)
    return key == _down1 || key == _down2;
  else
    return key == _down && !_pressed;
}

bool LinuxKeyboard::isPressed(Keysym_t key) {
  if ((key == KEY_CTRLL) || (key == KEY_CTRLR)) {
    return ((_state1 | _state2) & CTRL());
  }
  else if ((key == KEY_ALTL) || (key == KEY_ALTR)) {
    return ((_state1 | _state2) & ALT());
  }
  else
    return key == _pressed;
}

bool LinuxKeyboard::wentUp(Keysym_t key) {
  return key == _up;
}
} 
