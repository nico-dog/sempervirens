#define KEYSYMBOLS_CPP
#include <Keyboard/KeySymbols.hpp>

namespace sempervirens::device::keyboard {
char const* to_string(Keymod_t mod) {
  if (mod & SHIFT())     return "+SHIFT";
  else if (mod & LOCK()) return "+LOCK";
  else if (mod & CTRL()) return "+CTRL";
  else if (mod & ALT())  return "+ALT";
  else if (mod & NUML()) return "+NUML";
  return "NONE";
}
}
