#define CONTROLLER_CPP
#include <Controller/Controller.hpp>

namespace sempervirens::device::controller {
Controller::~Controller() {
  for (std::uint8_t i = 0; i < _index; ++i) {
    auto input = _inputs[i]._input;
    input->~Input();
  }
}

bool Controller::isAnyInputTriggered() {
  bool result = false;
  for (std::uint8_t i = 0; i < _index; ++i) {
    result |= _inputs[i]._input->invoke();
  }
  return result;
}

bool Controller::isChordTriggered() {
  bool result = true;
  for (std::uint8_t i = 0; i < _index; ++i) {
    result &= _inputs[i]._input->invoke();
  }
  return result;
}
} 
