#include <Logging/Logging.hpp>

namespace sempervirens::device::controller {
template <class Device, typename Function, typename Button>
bool DeviceInput<Device, Function, Button>::invoke() const {
  return (_device->*_function)(_button);
}

template <class Device, typename Function, typename Button>
void Controller::addInput(Device *device, Function function, Button button) {
  using Input_t = DeviceInput<Device, Function, Button>;

  static_assert(sizeof(Input_t) <= +InputStorage::MAX_SIZE,
                "Buffer is too small.");

  if (+_index == +Controller::MAX_INPUTS) {
    SEMPERVIRENS_WRN("Controller cannot hold more than %d inputs\n", +Controller::MAX_INPUTS);
    return;
  }

  void *memory = _inputs[_index]._memory;
  _inputs[_index]._input = new (memory) Input_t(device, function, button);
  ++_index;
}
} 
