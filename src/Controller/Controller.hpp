#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

namespace sempervirens::device::controller {
// base class for type erasure
class Input {
public:
  virtual ~Input() = default;
  virtual bool invoke() const = 0;
};
  
template <class Device, typename Function, typename Button>
class DeviceInput : public Input {
public:
  DeviceInput(Device* device, Function function, Button button)
      : _device{device}, _function{function}, _button{button} {}
  ~DeviceInput() = default;

  bool invoke() const override;
  
private:
  // vptr             // 8B
  Device* _device;    // 8B
  Function _function; // 16B for pointer to member functions
  Button _button;     // 4B (mouse::Button_t or keyboard::Keysym_t)
  char _padding[4];   // Pad up to 40B
};

struct InputStorage {
  static const std::uint8_t MAX_SIZE = 40;
  Input* _input{nullptr}; // 8B
  char _memory[MAX_SIZE]; // 40B
};

class Controller {
public:
  Controller() = default;
  ~Controller();

  template <class Device, typename Function, typename Button>
  void addInput(Device* device, Function function, Button button);

  // void processInput();
  bool isAnyInputTriggered();
  bool isChordTriggered();

private:
  static const std::uint8_t MAX_INPUTS = 3;
  InputStorage _inputs[MAX_INPUTS]; // 144B
  std::uint8_t _index{0};           // 1B
  char _padding[7];                 // Pad up to 152B
};
}
#include <Controller/Controller.inl>
#endif
