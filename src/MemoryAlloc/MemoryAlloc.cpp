#define MEMORYALLOC_CPP
#include <MemoryAlloc/MemoryAlloc.hpp>

namespace sempervirens::memoryalloc {
void* allocateHeap(std::size_t size) { return malloc(size); }

void deallocateHeap(void* ptr) { free(ptr); }

} 
