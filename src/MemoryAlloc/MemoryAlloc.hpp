#ifndef MEMORYALLOC_HPP
#define MEMORYALLOC_HPP

namespace sempervirens::memoryalloc {
void* allocateHeap(std::size_t size);
void deallocateHeap(void* ptr);
} 
#endif
