#ifndef MEMORYARENA_HPP
#define MEMORYARENA_HPP
#include <Macros/Build.hpp>

namespace sempervirens::memoryalloc {
// template <class AllocationPolicy, class ThreadPolicy, class
// BoundsCheckingPolicy, class MemoryTrackingPolicy, class MemoryTaggingPolicy>
template<class AllocationPolicy, class BoundsCheckingPolicy, class MemoryTrackingPolicy>
class MemoryArena {
public:
  MemoryArena(AllocationPolicy* allocator);
  ~MemoryArena() = default;

  MemoryArena(MemoryArena const&) = delete;
  MemoryArena& operator=(MemoryArena const&) = delete;
  MemoryArena(MemoryArena&&) = delete;
  MemoryArena& operator=(MemoryArena&&) = delete;

  void* allocate(std::size_t size, std::size_t alignment, char const* file, int line);
  void deallocate(void* ptr, std::size_t size);

#if SEMPERVIRENS_BUILD(UNITTESTING)
  static void Test();
#endif

private:
  AllocationPolicy* _allocator;
  // ThreadPolicy _threadGuard;
  BoundsCheckingPolicy _boundsChecking;
  MemoryTrackingPolicy _memoryTracking;
  // MemoryTaggingPolicy _memoryTagger;
};

} 
#include <MemoryAlloc/MemoryArena.inl>
#endif
