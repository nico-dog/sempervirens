#define MEMORYTRACKINGPOLICY_CPP
#include <MemoryAlloc/MemoryTrackingPolicy.hpp>

namespace sempervirens::memoryalloc {

DefaultMemoryTracking::~DefaultMemoryTracking() { SEMPERVIRENS_MSG("Net allocations: %d\n", NUM_ALLOC); }
  
std::uint32_t DefaultMemoryTracking::NUM_ALLOC = 0;

} 
