#ifndef BOUNDSCHECKINGPOLICY_HPP
#define BOUNDSCHECKINGPOLICY_HPP
#include <MemoryAlloc/MemoryHelperFcns.hpp>

namespace sempervirens::memoryalloc {
class VoidBoundsChecking {
public:
  inline void onAlloc(void* const ptr, std::size_t size) const {}
  inline void onDealloc(void* const ptr, std::size_t size) const {}
};

class DefaultBoundsChecking {
public:
  static const std::uint32_t GUARD_VALUE = 0xbcbcbcbc;
  static const std::size_t GUARD_SIZE = sizeof(GUARD_VALUE); 
  
  inline void onAlloc(void* const ptr, std::size_t size) const {
    APtr<char> p{ptr};
    Memset(p.asVoid(), GUARD_VALUE);
    p += (GUARD_SIZE + size);
    Memset(p.asVoid(), GUARD_VALUE);  
  }
  inline void onDealloc(void* const ptr, std::size_t size) const {
    APtr<char> p{ptr};
    p -= GUARD_SIZE;
    Memcheck(p.asVoid(), GUARD_VALUE);
    p += (GUARD_SIZE + size);
    Memcheck(p.asVoid(), GUARD_VALUE);
  }
};  

class ExtendedBoundsChecking {
public:
  static const std::uint32_t GUARD_VALUE = 0xbcbcbcbc;
  static const std::size_t GUARD_SIZE = sizeof(GUARD_VALUE); 
  static const std::uint32_t SIZE_SIZE = sizeof(std::uint32_t);
  static const std::size_t PTR_SIZE = sizeof(void*);
  
  void onAlloc(void* const ptr, std::size_t size) const;
  void onDealloc(void* const ptr, std::size_t size) const;
};  

} 
#endif
