#include <Logging/Logging.hpp>
#include <MemoryAlloc/BoundsCheckingPolicy.hpp>
#include <MemoryAlloc/MemoryTrackingPolicy.hpp>
#include <MemoryAlloc/MemoryHelperFcns.hpp>

#if SEMPERVIRENS_BUILD(UNITTESTING)
#include <MemoryAlloc/MemoryAlloc.hpp>
#include <MemoryAlloc/LinearAllocator.hpp>
#endif

namespace sempervirens::memoryalloc {
template<class AllocationPolicy, class BoundsCheckingPolicy, class MemoryTrackingPolicy>
MemoryArena<AllocationPolicy, BoundsCheckingPolicy, MemoryTrackingPolicy>::
MemoryArena(AllocationPolicy* allocator) : _allocator{allocator} {
  SEMPERVIRENS_MSG("Memory arena ctor:\n.. allocator begins at: %#x\n", _allocator->begin());
}

template<class AllocationPolicy, class BoundsCheckingPolicy, class MemoryTrackingPolicy>
void* MemoryArena<AllocationPolicy, BoundsCheckingPolicy, MemoryTrackingPolicy>::
allocate(std::size_t size, std::size_t alignment, const char* file, int line) {
  // Allocate for total size and offset based on bounds checking policy
  auto totalSize = size;
  auto offset = std::size_t{0};
  if constexpr (std::is_same_v<BoundsCheckingPolicy, DefaultBoundsChecking>) {
      totalSize += 2 * BoundsCheckingPolicy::GUARD_SIZE;
      offset = BoundsCheckingPolicy::GUARD_SIZE;
  }
  if constexpr (std::is_same_v<BoundsCheckingPolicy, ExtendedBoundsChecking>) {
      totalSize += 2 * BoundsCheckingPolicy::GUARD_SIZE
	+ BoundsCheckingPolicy::SIZE_SIZE
	+ BoundsCheckingPolicy::PTR_SIZE;
      offset = BoundsCheckingPolicy::GUARD_SIZE
	+ BoundsCheckingPolicy::SIZE_SIZE
	+ BoundsCheckingPolicy::PTR_SIZE;
  }
  auto ptr = _allocator->allocate(totalSize, alignment, offset);

  _boundsChecking.onAlloc(ptr, size);
  _memoryTracking.onAlloc();

  APtr<char> p{ptr};
  p += offset;
  return p.asVoid();
}

template <class AllocationPolicy, class BoundsCheckingPolicy, class MemoryTrackingPolicy>
void MemoryArena<AllocationPolicy, BoundsCheckingPolicy, MemoryTrackingPolicy>::
deallocate(void* ptr, std::size_t size) {

  _boundsChecking.onDealloc(ptr, size);
  _memoryTracking.onDealloc();
  
  _allocator->deallocate(ptr, size);
}
  

  
#if SEMPERVIRENS_BUILD(UNITTESTING)
template <class AllocationPolicy, class BoundsCheckingPolicy, class MemoryTrackingPolicy>
void MemoryArena<AllocationPolicy, BoundsCheckingPolicy, MemoryTrackingPolicy>::Test() {
  auto bufferSize = std::size_t{SEMPERVIRENS_B(128)};
  char* buffer[bufferSize] = {0};
  auto allocator = LinearAllocator{static_cast<void*>(buffer), bufferSize};
  //auto allocator = LinearAllocator{allocateHeap(bufferSize), bufferSize};

  //auto arena = MemoryArena<LinearAllocator, DefaultBoundsChecking, DefaultMemoryTracking>{&allocator};
  auto arena = MemoryArena<LinearAllocator, ExtendedBoundsChecking, DefaultMemoryTracking>{&allocator};
  
  struct MyType {
    int _value;
    MyType() { SEMPERVIRENS_MSG("MyType default ctor\n"); }
    explicit MyType(int val) : _value{val} {
      SEMPERVIRENS_MSG("MyType ctor\n");
    }
    ~MyType() { SEMPERVIRENS_MSG("MyType dtor\n"); }
  };

  struct MyPOD {
    int _value;
  };

  Block<MyType> myTypes = SEMPERVIRENS_NEW_ARRAY(MyType, arena, 2);
  allocator.printAlloc();
  myTypes._ptr[0]._value = 1;
  myTypes._ptr[1]._value = 2;
  allocator.printAlloc();

  Block<MyPOD> myPOD = SEMPERVIRENS_NEW(MyPOD, arena, 3);
  allocator.printAlloc();

  Block<MyPOD> myPODs = SEMPERVIRENS_NEW_ARRAY(MyPOD, arena, 2);
  allocator.printAlloc();
  myPODs._ptr[0]._value = 4;
  myPODs._ptr[1]._value = 5;
  allocator.printAlloc();


  SEMPERVIRENS_MSG("Deallocating %d bytes at %#x\n", myTypes._size, myTypes._ptr);
  SEMPERVIRENS_DELETE_ARRAY(myTypes, arena);
  
  SEMPERVIRENS_MSG("Deallocating %d bytes at %#x\n", myPODs._size, myPODs._ptr);
  SEMPERVIRENS_DELETE_ARRAY(myPODs, arena);
  SEMPERVIRENS_MSG("Deallocating %d bytes at %#x\n", myPOD._size, myPOD._ptr);
  SEMPERVIRENS_DELETE(myPOD, arena);
  //SEMPERVIRENS_MSG("Deallocating %d bytes at %#x\n", myTypes._size, myTypes._ptr);
  //SEMPERVIRENS_DELETE_ARRAY(myTypes, arena);

  allocator.reset();
  allocator.printAlloc();
}
#endif
} 
