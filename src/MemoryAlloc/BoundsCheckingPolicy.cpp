#define BOUNDSCHECKINGPOLICY_CPP
#include <MemoryAlloc/BoundsCheckingPolicy.hpp>
//#include <Logging/Logger.hpp>

namespace sempervirens::memoryalloc {
static void* head_alloc = nullptr;
static void* tail_alloc = nullptr;
static std::mutex mutex_alloc{};

void ExtendedBoundsChecking::onAlloc(void* const ptr, std::size_t size) const {
  std::lock_guard<std::mutex> lock(mutex_alloc);
  // add alloc to list, set guards, size of alloc and pointer to next alloc
  APtr<char> p{ptr};
  if (head_alloc) 
    Memset(tail_alloc, ptr);
  else 
    head_alloc = ptr;
  Memset(p.asVoid(), GUARD_VALUE);
  p += GUARD_SIZE;
  Memset(p.asVoid(), size);
  p += SIZE_SIZE;
  tail_alloc = p.asVoid();
  Memset(p.asVoid(), head_alloc);  
  p += (PTR_SIZE + size);
  Memset(p.asVoid(), GUARD_VALUE);  
  
  // Check all guards
  APtr<char> p_front{head_alloc};
  auto p_back = p_front;
  p_back += GUARD_SIZE;
  auto size_alloc = *(static_cast<std::uint32_t*>(p_back.asVoid()));
  p_back += SIZE_SIZE;
  auto next_alloc = *(static_cast<void**>(p_back.asVoid()));
  p_back += (PTR_SIZE + size_alloc);
  do {
    Memcheck(p_front.asVoid(), GUARD_VALUE);
    Memcheck(p_back.asVoid(), GUARD_VALUE);
    p_front = next_alloc;
    p_back = p_front;
    p_back += GUARD_SIZE;
    size_alloc = *(static_cast<std::uint32_t*>(p_back.asVoid()));
    p_back += SIZE_SIZE;
    next_alloc = *(static_cast<void**>(p_back.asVoid()));
    p_back += (PTR_SIZE + size_alloc);
  } while (next_alloc != head_alloc);
}  

void ExtendedBoundsChecking::onDealloc(void* const ptr, std::size_t size) const {
  std::lock_guard<std::mutex> lock(mutex_alloc);
  // check all guards
  // while traversing the list we find the previous allocation and the pointer
  // its next allocation should point to for book-keeping
  void* alloc = nullptr;
  void* prev_alloc = nullptr;
  void* prev_alloc_ptr = nullptr;
  APtr<char> p_front{head_alloc};
  APtr<char> p_back{head_alloc};
  auto next_alloc = head_alloc;
  std::uint32_t size_alloc = 0;
  do {
    p_front = next_alloc;
    p_back = p_front;
    p_back += GUARD_SIZE;
    size_alloc = *(static_cast<std::uint32_t*>(p_back.asVoid()));
    p_back += SIZE_SIZE;
    next_alloc = *(static_cast<void**>(p_back.asVoid()));
    p_back += (PTR_SIZE + size_alloc);
    Memcheck(p_front.asVoid(), GUARD_VALUE);
    Memcheck(p_back.asVoid(), GUARD_VALUE);
    //SEMPERVIRENS_MSG("current alloc block at %#x\n", p_front.asVoid());
    //SEMPERVIRENS_MSG("next alloc block at %#x\n", next_alloc);
    APtr<char> data_ptr{next_alloc};
    data_ptr += (GUARD_SIZE + SIZE_SIZE + PTR_SIZE);
    //SEMPERVIRENS_MSG("next alloc data at %#x\n", data_ptr.asVoid());
    if (data_ptr.asVoid() == ptr) {
      //SEMPERVIRENS_MSG("found data\n");
      alloc = next_alloc;
      prev_alloc = p_front.asVoid();
      data_ptr -= PTR_SIZE;
      prev_alloc_ptr = *(static_cast<void**>(data_ptr.asVoid()));
    }
  } while (next_alloc != head_alloc);

  // remove alloc ptr from list
  if (alloc == head_alloc) {
    if (prev_alloc_ptr == head_alloc) {
      head_alloc = nullptr;
      tail_alloc = nullptr;
    }
    else head_alloc = prev_alloc_ptr;
  }
  APtr<char> p{prev_alloc};
  p += (GUARD_SIZE + SIZE_SIZE);
  Memset(p.asVoid(), prev_alloc_ptr);
}
  
}
