#ifndef MEMORYTRACKINGPOLICY_HPP
#define MEMORYTRACKINGPOLICY_HPP
#include <Logging/Logging.hpp>
#include <MemoryAlloc/MemoryHelperFcns.hpp>

namespace sempervirens::memoryalloc {
class VoidMemoryTracking {
public:
  inline void onAlloc() const {}
  inline void onDealloc() const {}
};

class DefaultMemoryTracking {
public:
  ~DefaultMemoryTracking();

  inline void onAlloc() const { ++NUM_ALLOC; }
  inline void onDealloc() const { --NUM_ALLOC; }

  static std::uint32_t NUM_ALLOC;
};

class ExtendedMemoryTracking {};  
  // TO BE IMPLEMENTED
} 
#endif
