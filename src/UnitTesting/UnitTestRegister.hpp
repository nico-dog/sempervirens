#ifndef UNITTESTREGISTER_HPP
#define UNITTESTREGISTER_HPP
#include <UnitTesting/UnitTest.hpp>
#include <Logging/Logging.hpp>

namespace sempervirens::unittesting {
class UnitTestRegister {
public:
  UnitTestRegister() = default;
  ~UnitTestRegister() = default;
  UnitTestRegister(UnitTestRegister const&) = delete;
  UnitTestRegister& operator=(UnitTestRegister const&) = delete;
  
  void push(UnitTest test);
  void run();

private:
  //class UnitTestRegisterImpl;
  //std::unique_ptr<UnitTestRegisterImpl, void (*)(UnitTestRegisterImpl*)> _pImpl;
  std::vector<UnitTest> _unitTests;
  sempervirens::logging::ConsoleLog _log;
};

#define SEMPERVIRENS_TESTTYPE(type) []() { type::Test(); }
#define SEMPERVIRENS_TESTCALL(callable, ...)                                       \
  sempervirens::unittesting::createUnitTest(callable, ##__VA_ARGS__)  
} 
#endif
