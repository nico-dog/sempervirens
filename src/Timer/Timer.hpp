#ifndef TIMER_HPP
#define TIMER_HPP
#include <Macros/Platform.hpp>

#if SEMPERVIRENS_PLATFORM(UNIX)
#include <Timer/Platform/Linux/LinuxTimer.hpp>
#if SEMPERVIRENS_BUILD(DEBUG)
#include <Logging/Logging.hpp>
// use variadic arg to capture cuda chevron call syntax
#define SEMPERVIRENS_TIME(...) \
    do { \
    	auto start = sempervirens::time::getTimeInMus(); \
    	__VA_ARGS__; \
    	auto stop = sempervirens::time::getTimeInMus(); \
	SEMPERVIRENS_MSG("%s ran in %fs\n", #__VA_ARGS__, (stop - start) * 1E-6); \
    } while (0)
#else
#define SEMPERVIRENS_TIME(...) \
    do { \
	__VA_ARGS__; \
    } while (0)
#endif
#endif

#endif
