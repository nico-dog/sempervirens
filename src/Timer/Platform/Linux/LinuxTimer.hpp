#ifndef LINUXTIMER_HPP
#define LINUXTIMER_HPP
#include <time.h>
namespace sempervirens::timer {
std::uint64_t getTimeInMus();
}

#endif
