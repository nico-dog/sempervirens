#define LINUXTIMER_CPP
#include <Timer/Platform/Linux/LinuxTimer.hpp>

#include <Logging/Logging.hpp>
namespace sempervirens::timer {
std::uint64_t getTimeInMus() {
  timespec time;
  // clock_gettime(CLOCK_MONOTONIC, &time);
  clock_gettime(CLOCK_REALTIME, &time);
  return 1000000 * time.tv_sec + time.tv_nsec / 1000;
  // return time.tv_sec;

  SEMPERVIRENS_MSG("here %d, %d\n", time.tv_sec, time.tv_nsec);
}
} // namespace sempervirens::timer
