#ifndef APPLICATION_HPP
#define APPLICATION_HPP
#include <SempervirensVersion.hpp>
#include <Config/ConfigSetting.hpp>
#include <EventSystem/Event.hpp>
#include <Keyboard/Keyboard.hpp>
#include <MemoryAlloc/BoundsCheckingPolicy.hpp>
#include <MemoryAlloc/MemoryTrackingPolicy.hpp>
#include <MemoryAlloc/LinearAllocator.hpp>
#include <MemoryAlloc/MemoryArena.hpp>
#include <Mouse/Mouse.hpp>
#include <Renderer/Renderer.hpp>
#include <Window/Window.hpp>

namespace sempervirens::app {
class Application {
public:
  Application();
  virtual ~Application() { SEMPERVIRENS_MSG("Application dtor\n"); };

  Application(Application const &) = delete;
  Application &operator=(Application const &) = delete;

  inline void setWindow(Window_t *window) { _window = window; }
  inline void setKeyboard(Keyboard_t *keyboard) { _keyboard = keyboard; }
  inline void setMouse(Mouse_t *mouse) { _mouse = mouse; }
  inline void setRenderer(Renderer_t *renderer) { _renderer = renderer; }
  void run();
  void onEvent(sempervirens::eventsystem::Event &event);

  void onWindowCloseEvent(sempervirens::eventsystem::WindowCloseEvent const& event);

private:
  // vptr                         // 8B
  Window_t* _window{nullptr};     // 8B
  Keyboard_t* _keyboard{nullptr}; // 8B
  Mouse_t* _mouse{nullptr};       // 8B
  Renderer_t* _renderer{nullptr}; // 8B
  bool _appIsRunning{true};       // 1B
  char _padding[7];               // Pad up to 48B
};

// TO BE DEFINED BY CLIENT FOR ENTRYPOINT.
using namespace sempervirens::memoryalloc;
#if SEMPERVIRENS_BUILD(RELEASE)
using Arena_t = MemoryArena<LinearAllocator, VoidBoundsChecking, VoidMemoryTracking>;
#else
using Arena_t = MemoryArena<LinearAllocator, DefaultBoundsChecking, DefaultMemoryTracking>;
#endif
Block<Application> createApplication(Arena_t &arena);
} 
#endif
