#define APPLICATION_CPP
#include <Application/Application.hpp>
#include <Controller/Controller.hpp>
#include <Logging/Logging.hpp>
#include <Timer/Timer.hpp>
using namespace sempervirens::timer;
using namespace sempervirens::window;
using namespace sempervirens::eventsystem;
using namespace sempervirens::device::controller;

namespace sempervirens::config {
extern ConfigSettingType<float> g_UpdateRate;
extern ConfigSettingType<int> g_Verbosity;  
}


namespace sempervirens::app {
Application::Application() {
  SEMPERVIRENS_MSG("Application ctor, size %d\n", sizeof(Application));
  //SEMPERVIRENS_LISTEN(this, WindowCloseEvent);
}

void Application::run() {
  auto updatesPerSecond = sempervirens::config::g_UpdateRate._value;
  //auto updatesPerSecond = 25.;
  auto updatePeriod = 1.E6 / updatesPerSecond;
  auto maxSkipUpdateCount = 5;
  auto skipUpdateCount = 0;
  //auto delta = 0.;
  SEMPERVIRENS_MSG("Sempervirens application is running with parameters:\n \
.. application speed: %f updates/sec\n \
.. lowest speed: %f updates/sec\n", updatesPerSecond, 5000. / updatePeriod);

  auto nextUpdateTime = getTimeInMus();
  auto updateCount = 0.;
  auto frameCount = 0.;
  auto refTime = nextUpdateTime;

  // Testing some application controllers
  /*
  Controller testSelectController;
  testSelectController.addInput(_keyboard, &Keyboard_t::isPressed, KEY_CTRLL);
  testSelectController.addInput(_mouse, &Mouse_t::wentDown, Button_t::Left);
  */

  // engine defined controllers
  Controller closingController;
  closingController.addInput(_keyboard, &Keyboard_t::isPressed, KEY_CTRLL);
  closingController.addInput(_keyboard, &Keyboard_t::wentDown, KEY_q);

  Controller verbosityController;
  verbosityController.addInput(_keyboard, &Keyboard_t::isPressed, KEY_CTRLL);
  verbosityController.addInput(_keyboard, &Keyboard_t::wentDown, KEY_v);

  /*
  // client controllers
  Controller testController1;
  testController1.addInput(_keyboard, &Keyboard_t::isPressed, KEY_CTRLL);
  testController1.addInput(_keyboard, &Keyboard_t::wentDown, KEY_x);
  testController1.addInput(_keyboard, &Keyboard_t::wentDown, KEY_s);
  Controller testController2;
  testController2.addInput(_keyboard, &Keyboard_t::isPressed, KEY_ALTR);
  testController2.addInput(_keyboard, &Keyboard_t::wentDown, KEY_x);
  testController2.addInput(_keyboard, &Keyboard_t::wentDown, KEY_s);

  Controller testController3;
  testController3.addInput(_keyboard, &Keyboard_t::isPressed, KEY_CTRLL);
  testController3.addInput(_keyboard, &Keyboard_t::isPressed, KEY_ALTL);
  testController3.addInput(_keyboard, &Keyboard_t::isPressed, KEY_l);
  Controller testController4;
  testController4.addInput(_keyboard, &Keyboard_t::isPressed, KEY_CTRLL);
  testController4.addInput(_keyboard, &Keyboard_t::isPressed, KEY_x);
  Controller testController5;
  testController5.addInput(_keyboard, &Keyboard_t::isPressed, KEY_ALTL);
  testController5.addInput(_keyboard, &Keyboard_t::wentDown, KEY_x);
  
  Controller testController6;
  testController6.addInput(_keyboard, &Keyboard_t::isPressed, KEY_p);
  testController6.addInput(_keyboard, &Keyboard_t::wentDown, KEY_d);
  testController6.addInput(_keyboard, &Keyboard_t::wentUp, KEY_u);
  */

  Controller testController1;
  testController1.addInput(_mouse, &Mouse_t::wentDown, Button_t::Left);
  testController1.addInput(_mouse, &Mouse_t::wentDown, Button_t::Right);
  testController1.addInput(_mouse, &Mouse_t::wentDown, Button_t::Scroll);
  Controller testController2;
  testController2.addInput(_mouse, &Mouse_t::wentUp, Button_t::Left);
  testController2.addInput(_mouse, &Mouse_t::wentUp, Button_t::Right);
  testController2.addInput(_mouse, &Mouse_t::wentUp, Button_t::Scroll); 
  Controller testController3;
  testController3.addInput(_mouse, &Mouse_t::wentDown, Button_t::ScrollDown);
  testController3.addInput(_mouse, &Mouse_t::wentDown, Button_t::ScrollUp);
  Controller testController4;
  testController4.addInput(_mouse, &Mouse_t::isPressed, Button_t::Left);
  testController4.addInput(_mouse, &Mouse_t::isPressed, Button_t::Right);
  
  while (_appIsRunning) {
    auto now = getTimeInMus();

    // poll input devices
    if (_window->processInput()) {

    

      // poll controllers
      //if (testSelectController.isChordTriggered()) SEMPERVIRENS_MSG("Enabling selection\n");

      if (closingController.isChordTriggered()) {
	auto event = WindowCloseEvent{};
	SEMPERVIRENS_MSG("Closing App\n");
	SEMPERVIRENS_SIGNAL(event);
      }
      if (verbosityController.isChordTriggered()) {
	SEMPERVIRENS_MSG("Reduce verbosity\n");
	sempervirens::config::g_Verbosity = !sempervirens::config::g_Verbosity;
	SEMPERVIRENS_MSG("Increase verbosity\n");
      }
      /*
      if (testController1.isChordTriggered()) {
	SEMPERVIRENS_MSG("CTRL sequence detected\n");
      }
      if (testController2.isChordTriggered()) {
	SEMPERVIRENS_MSG("ALT sequence detected\n");
      }
      if (testController3.isChordTriggered()) {
	SEMPERVIRENS_MSG("CTRL+ALT key pressed\n");
      }
      if (testController4.isChordTriggered()) {
	SEMPERVIRENS_MSG("CTRL key down\n");
      }
      if (testController5.isChordTriggered()) {
	SEMPERVIRENS_MSG("ALT key down\n");
      }
      if (testController6.isAnyInputTriggered()) {
	SEMPERVIRENS_MSG("key down or pressed or up\n");
      }
      */

      if (testController1.isAnyInputTriggered()) {
	SEMPERVIRENS_MSG("Mouse button down\n");
      }
      if (testController2.isAnyInputTriggered()) {
	SEMPERVIRENS_MSG("Mouse button up\n");
      }
      if (testController3.isAnyInputTriggered()) {
	SEMPERVIRENS_MSG("Mouse scroll\n");
      }
      if (testController4.isAnyInputTriggered()) {
	SEMPERVIRENS_MSG("Mouse button is pressed\n");
      }
    }      
    // application update 
    while (now >= nextUpdateTime && skipUpdateCount < maxSkipUpdateCount) {
      // update()
      nextUpdateTime += updatePeriod;
      ++skipUpdateCount;
      ++updateCount;
    }
    skipUpdateCount = 0;

    //delta = (now + updatePeriod - nextUpdateTime) / updatePeriod;
    //_renderer->update(delta)
    _renderer->render();

    _window->update();

 

    ++frameCount;
    if (auto duration = (getTimeInMus() - refTime); duration >= 1000000) {
      SEMPERVIRENS_MSG("Updates/s = %f\n", updateCount * 1000000 / duration);
      SEMPERVIRENS_MSG("FPS = %f\n", frameCount * 1000000 / duration);
      refTime = getTimeInMus();
      updateCount = 0;
      frameCount = 0;
    }
  }
}

void Application::onEvent(Event& event) {
  if (event.type() == EventType::WindowClosed) {
    _appIsRunning = false;
    SEMPERVIRENS_MSG("Application received WindowCloseEvent\n");
    return;
  }
}

void Application::onWindowCloseEvent(WindowCloseEvent const& event) {
  _appIsRunning = false;
  SEMPERVIRENS_MSG("Application received WindowCloseEvent from new messaging system\n");
  return;
}
  
} 
