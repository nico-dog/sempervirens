#ifndef ENTRYPOINT_HPP
#define ENTRYPOINT_HPP
#include <Application/Application.hpp>
#include <Logging/Logging.hpp>
#include <Config/ConfigSetting.hpp>

using namespace sempervirens::app;
using namespace sempervirens::memoryalloc;

// DEFINED BY CLIENT.
extern Block<sempervirens::app::Application> sempervirens::app::createApplication(Arena_t& arena);

int main(int argc, char* argv[]) {
#if SEMPERVIRENS_BUILD(DEBUG)
  auto log = sempervirens::logging::ConsoleLog{};
#endif

  SEMPERVIRENS_MSG("Sempervirens engine version: %s\n", getSempervirensVersion().c_str());
  SEMPERVIRENS_MSG("Default config settings:\n");
  sempervirens::config::printConfig();
  // SEMPERVIRENS_MSG("config file: " << argv[1]);

  // Arena for application and physical devices.
  // TestApp <- Application = 48B
  // Window = 64B
  // Keyboard = 56B
  // Mouse = 20B
  // Renderer = 312B
  // Total = 500B

  // set up linear memory arena
  auto size = SEMPERVIRENS_B(512);
  void* ptr = allocateHeap(size);
  auto allocator = LinearAllocator{ptr, size};
  auto arena = Arena_t{&allocator};
  
  // allocate client app
  auto app = createApplication(arena);

  // allocate input devices
  auto window = SEMPERVIRENS_NEW(Window_t, arena);
  app._ptr->setWindow(window._ptr);
  auto keyboard = SEMPERVIRENS_NEW(Keyboard_t, arena);
  app._ptr->setKeyboard(keyboard._ptr);
  auto mouse = SEMPERVIRENS_NEW(Mouse_t, arena);
  app._ptr->setMouse(mouse._ptr);

  // allocate systems
  auto renderer = SEMPERVIRENS_NEW(Renderer_t, arena);
  app._ptr->setRenderer(renderer._ptr);


  //SEMPERVIRENS_ADD_LISTENER(app._ptr, g_WindowCloseEventListeners);
  SEMPERVIRENS_ADD_LISTENER(app._ptr, sempervirens::eventsystem::WindowCloseEvent);

 #if SEMPERVIRENS_BUILD(DEBUG) 
  allocator.printAlloc();
#endif
  
  // run client app
  app._ptr->run();

  // delete systems, input devices, and app
  SEMPERVIRENS_DELETE(renderer, arena);
  SEMPERVIRENS_DELETE(mouse, arena);
  SEMPERVIRENS_DELETE(keyboard, arena);
  SEMPERVIRENS_DELETE(window, arena);
  SEMPERVIRENS_DELETE(app, arena);

  return 0;
}

#endif
