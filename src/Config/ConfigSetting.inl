#ifndef CONFIGSETTING_INL
#define CONFIGSETTING_INL

#include <Logging/Logging.hpp>

namespace sempervirens::config {
  
template<typename T>
ConfigSettingType<T>::ConfigSettingType(char const* name, char const* synopsis, T initialValue, T minValue, T maxValue) :
  _name{name}, _synopsis{synopsis}, _value{initialValue}, _min{minValue}, _max{maxValue} {
  AddToList(this);
}

template<typename T>  
ConfigSettingType<T>::~ConfigSettingType() { RemoveFromList(this); }

template<typename T>  
ConfigSettingType<T>& ConfigSettingType<T>::operator=(T value) {
  if (value < _min) {
    if constexpr (std::is_same_v<T, int>) 
       SEMPERVIRENS_WRN("Could not set value %d for setting \"%s\" because it is smaller than the minimum. It will be set to %d.", value, _name, _min);
    if constexpr (std::is_same_v<T, float>) 
       SEMPERVIRENS_WRN("Could not set value %f for setting \"%s\" because it is smaller than the minimum. It will be set to %f.", value, _name, _min);
    value = _min;
  }
  else if (value > _max) {
    if constexpr (std::is_same_v<T, int>) 
       SEMPERVIRENS_WRN("Could not set value %d for setting \"%s\" because it is greater than the maximum. It will be set to %d.", value, _name, _max);
    if constexpr (std::is_same_v<T, float>) 
       SEMPERVIRENS_WRN("Could not set value %f for setting \"%s\" because it is greater than the maximum. It will be set to %f.", value, _name, _max);
    value = _max;
  }
  _value = value;
  return *this;  
}

template<typename T>  
void ConfigSettingType<T>::print() {
  if constexpr (std::is_same_v<T, int>) 
     SEMPERVIRENS_MSG("%s, %s, value: %d (min: %d, max: %d)\n", _name, _synopsis, _value, _min, _max);
  if constexpr (std::is_same_v<T, float>) 
     SEMPERVIRENS_MSG("%s, %s, value: %f (min: %f, max: %f)\n", _name, _synopsis, _value, _min, _max);
}
  
}
#endif
