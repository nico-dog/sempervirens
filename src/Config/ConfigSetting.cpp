#define CONFIGSETTING_CPP
#include <Config/ConfigSetting.hpp>

//char forceLinkageConfig = 0;

namespace sempervirens::config {

static ConfigSetting* head_conf = nullptr;
static ConfigSetting* tail_conf = nullptr;

ConfigSettingType<int> g_Verbosity("Verbosity", "Defines the verbosity level of logging operations", 1, 0, 2);
ConfigSettingType<float> g_UpdateRate("Update rate", "Number of updates per second", 25., 0., 30.);
  
void ConfigSetting::AddToList(ConfigSetting* setting) {
  if (head_conf) {
    tail_conf->_next = setting;
    tail_conf = setting;
  } else {
    head_conf = setting;
    tail_conf = setting;
  }
}

void ConfigSetting::RemoveFromList(ConfigSetting* setting) {
  if (setting == head_conf) {
    if (setting == tail_conf) {
      head_conf = nullptr;
      tail_conf = nullptr;
    } else {
      head_conf = head_conf->_next;
    }
  }
  else {
    auto prev = head_conf;
    while (prev->_next != setting) {
      prev = prev->_next;
    }
    if (setting == tail_conf) {
      prev->_next = nullptr;
      tail_conf = prev;
    } else {
      prev->_next = setting->_next;
    }
  }
}

void printConfig() {
  auto config = head_conf;
  do {
    config->print();
    config = config->_next;
  } while (config);  

}
  
}

