#ifndef CONFIGSETTING_HPP
#define CONFIGSETTING_HPP

namespace sempervirens::config {

class ConfigSetting {
public:
  virtual ~ConfigSetting() = default;
  virtual void print() {};
  ConfigSetting* _next;
  
protected:
  ConfigSetting() = default;
  static void AddToList(ConfigSetting*);
  static void RemoveFromList(ConfigSetting*);
};

template<typename T>
class ConfigSettingType : public ConfigSetting {
public:
  ConfigSettingType(char const* name, char const* synopsis, T initialValue, T minValue, T maxValue);
  ~ConfigSettingType();
  ConfigSettingType& operator=(T value);
  inline operator T() const { return _value; }
  void print() override;
  char const* _name;
  char const* _synopsis;
  T _value;
  T _min;
  T _max;
};

void printConfig();
}
#include <Config/ConfigSetting.inl>
#endif
