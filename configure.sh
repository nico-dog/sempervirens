#!/bin/sh
config=$1
platform=$2
generator=''
if [ $platform = "Linux" ]
then
    generator="Unix Makefiles"
elif [ $platform = "apple" ]
then
    generator="Xcode"
else
    echo "unrecognized platform"
    return
fi

builddir="build-Sempervirens-"$config'-'$platform
rm -rf ../$builddir
mkdir ../$builddir && cd ../$builddir
cmake ../Sempervirens -G "$generator" --log-context -DCMAKE_BUILD_TYPE="$config" -DGL:BOOL=True
