#include <MemoryAlloc/MemoryArena.hpp>
#include <MemoryAlloc/LinearAllocator.hpp>
#include <UnitTesting/UnitTestRegister.hpp>

using namespace sempervirens::unittesting;
using namespace sempervirens::memoryalloc;

using LinearArena = MemoryArena<LinearAllocator, ExtendedBoundsChecking, DefaultMemoryTracking>;

int main() {

  auto reg = UnitTestRegister{};
  reg.push(SEMPERVIRENS_TESTTYPE(LinearArena));
  reg.run();
  
  return 0;
}
