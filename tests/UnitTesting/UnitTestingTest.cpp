#include <UnitTesting/UnitTestRegister.hpp>
#include <Logging/Logging.hpp>
#include <fstream>

using namespace sempervirens::unittesting;

// A Sempervirens type with embedded Test method
class SempervirensType
{  
public:
  void print() const { SEMPERVIRENS_MSG("%d\n", _val); }
  void add(int n) { SEMPERVIRENS_MSG("adding %d\n", n); _val += n; }
private:
  int _val{0};

#if SEMPERVIRENS_BUILD(UNITTESTING)
public:
  void static Test()
  {
    SempervirensType t;
    t.print();
    t.add(2);
    t.print();
  }
#endif
};

// Another type for which we want to test member functions
class OtherType
{  
public:
  void print() const { SEMPERVIRENS_MSG("%d\n", _val); }
  void add(int n) { SEMPERVIRENS_MSG("adding %d\n", n); _val += n; }
private:
  int _val{0};
};

// Other callables
auto closure = [](int val){ SEMPERVIRENS_MSG("closure with arg: %d\n", val); };
void freeFunctionNoArg() { SEMPERVIRENS_MSG("free function with no arg\n"); }
void freeFunctionOneArg(int val) { SEMPERVIRENS_MSG("free function with one arg: %d\n", val); }

// A test wrapper for additional control (e.g. code injection)
class TestWrapper
{
public:
  TestWrapper(std::string filename) : _file{std::make_unique<std::ofstream>(std::move(filename))} {}
  void operator()() const
  {
    SEMPERVIRENS_MSG("Logging test result to file\n");
    *_file << "test result\n";
  }
private:
  std::unique_ptr<std::ofstream> _file;
};

int main()
{
  auto reg = UnitTestRegister();
  reg.push(SEMPERVIRENS_TESTTYPE(SempervirensType));
  OtherType t;
  reg.push(SEMPERVIRENS_TESTCALL(&OtherType::add, &t, 10));
  reg.push(SEMPERVIRENS_TESTCALL(&OtherType::print, &t));
  reg.push(SEMPERVIRENS_TESTCALL(closure, 2));
  reg.push(SEMPERVIRENS_TESTCALL(freeFunctionNoArg));
  reg.push(SEMPERVIRENS_TESTCALL(freeFunctionOneArg, 10));
  reg.push(TestWrapper{"TestLog.txt"});
  reg.run();
  return 0;
}

