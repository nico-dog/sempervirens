#include <Logging/Logging.hpp>
#include <UnitTesting/UnitTestRegister.hpp>

using namespace sempervirens::unittesting;
using namespace sempervirens::logging;

int main(int argc, char* argv[]) {

  //extern char forceLinkageConfig;
  //forceLinkageConfig = 0;
  
  auto reg = UnitTestRegister{};
  reg.push(SEMPERVIRENS_TESTTYPE(ConsoleLog));
  reg.run();

  return 0;
}
