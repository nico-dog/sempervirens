#include <Window/Platform/Linux/Backend/XGLWindow.hpp>
#include <UnitTesting/UnitTestRegister.hpp>

using namespace sempervirens::unittesting;
using namespace sempervirens::window;

int main(int argc, char* argv[]) {
  
  auto reg = UnitTestRegister{};
  reg.push(SEMPERVIRENS_TESTTYPE(XGLWindow));
  reg.run();
  return 0;
}
