#!/bin/sh

config=$1
platform=$2
generator=''

if [ $platform = "Linux" ]
then
    generator="Unix Makefiles"
elif [ $platform = "apple" ]
then
    generator="Xcode"
else
    echo "unrecognized platform"
    return
fi

builddir="build-Sempervirens-"$config'-'$platform
cd ../$builddir

cmake --log-context ../Sempervirens -DCMAKE_BUILD_TYPE=$config -DCMAKE_TOOLCHAIN_FILE=./conan_paths.cmake 
cmake --build . --config $config --parallel 8

